#include <stdint.h>
#include <SDL/SDL.h>
#include <Pixels.hpp>

//If white, put a white pixel, else a black one
//The surface must have 32 bits colors
//This function doesn't lock the surface
//TODO : if scale > 1, use SDL_FillRect (= quicker)
void putPixel(SDL_Surface *surface, int x, int y, Color color, int scale)
{
	for(int i = 0 ; i < scale ; i++)
	{
		for(int j = 0 ; j < scale ; j++)
		{
			*(uint32_t*) ((uint8_t*)surface->pixels + (y*scale + j) * surface->pitch + (x*scale + i) * surface->format->BytesPerPixel) = SDL_MapRGB(surface->format, color.r, color.g, color.b);
		}
	}
}
