#include <iostream>
#include <string>
#include <map>
#include <sstream>
#include <fstream>

#include <Ini.hpp>

using namespace std;

Ini::Ini()
{

}

Ini::Ini(string path)
{
	load(path);
}

Ini::~Ini()
{

}

bool Ini::load(string path)
{
	string prefix = "";
	ifstream file(path.c_str());
	string line;

	while(getline(file, line))
	{
		istringstream iss(line);
		string str;
		int pos = 0;

		if(line.empty() || line[0] == '#')
			continue;

		iss >> str;
		pos = str.find_first_of('[');
		if(pos == string::npos)
		{
			string key = str;
			iss >> str;
			if(str != "=")
			{
				pairs.clear();
				cout << "Error reading " << path << ": missing =" << endl;
				return false;
			}

			iss >> str;
			pairs[prefix][key] = str;
		}
		else
		{
			pos = str.find_first_of(']');
			if(pos == string::npos)
			{
				pairs.clear();
				cout << "Error reading " << path << ": closing bracket missing" << endl;
				return false;
			}

			prefix = str.substr(1, pos - 1);
		}
	}

	//Note : valide
	//pairs[""] = "val";
	//cout << pairs[""] << endl; → Affichera bien val

	return true;
}

void Ini::load_default(IniMap &inimap)
{
	IniMap::iterator it;
	for(it = inimap.begin() ; it != inimap.end() ; it++)
	{
		string cat = it->first;
		IniStringMap &inner_map = it->second;
		IniStringMap::iterator it2;
		for(it2 = inner_map.begin() ; it2 != inner_map.end() ; it2++)
		{
			string key = it2->first, val = it2->second;
			if(!pairs.count(cat) || !pairs[cat].count(key))
				pairs[cat][key] = val;
		}
	}
}

string Ini::get(string cat, string key)
{
	//Note : si l'on fait "return pairs[key];" dans le cas où l'élément n'existe pas, "" est bien renvoyé, mais la fonction Ini::empty() ne fonctionne plus après un appel à Ini::get() (créer un élément vide ?)
	if(pairs.count(cat)) //Renvoie 1 si la clé existe, 0 sinon
	{
		if(pairs[cat].count(key))
			return pairs[cat][key];
		else
			return "";
	}
	else
		return "";
}

void Ini::add(string cat, string key, string val)
{
	pairs[cat][key] = val;
}

bool Ini::empty()
{
	return pairs.empty();
}

void Ini::display()
{
	IniMap::iterator it;
	for(it = pairs.begin() ; it != pairs.end() ; it++)
	{
		string cat = it->first;
		if(cat != "")
			cout << "[" << cat << "]" << endl;

		IniStringMap &inner_map = it->second;
		IniStringMap::iterator it2;
		for(it2 = inner_map.begin() ; it2 != inner_map.end() ; it2++)
		{
			string key = it2->first, val = it2->second;
			cout << key << " = " << val << endl;
		}

		cout << endl;
	}
}

//Semble effacer le contenu précédent, donc ok
void Ini::save(string path)
{
	ofstream file(path.c_str());

	IniMap::iterator it;
	for(it = pairs.begin() ; it != pairs.end() ; it++)
	{
		string cat = it->first;
		file << "[" << cat << "]" << endl;

		IniStringMap &inner_map = it->second;
		IniStringMap::iterator it2;
		for(it2 = inner_map.begin() ; it2 != inner_map.end() ; it2++)
		{
			string key = it2->first, val = it2->second;
			file << key << " = " << val << endl;
		}

		file << endl;
	}
}
