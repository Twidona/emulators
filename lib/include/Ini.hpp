#ifndef _INI_HPP
#define _INI_HPP

#include <string>
#include <map>

//Attention : pour une map dans une map, séparer les '>' consécutifs par un espace
typedef std::map<std::string, std::string> IniStringMap;
typedef std::map< std::string, IniStringMap > IniMap;

class Ini
{
	public:
		Ini();
		Ini(std::string path);
		~Ini();
		bool load(std::string path);
		void load_default(IniMap &inimap);
		std::string get(std::string cat, std::string key);
		void add(std::string cat, std::string key, std::string val);
		bool empty();
		void display();
		void save(std::string path);

	private:
		IniMap pairs;
};

#endif
