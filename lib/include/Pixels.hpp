#ifndef _PIXELS_HPP
#define _PIXELS_HPP

#include <stdint.h>
#include <SDL/SDL.h>

#include <Color.hpp>

void putPixel(SDL_Surface *surface, int x, int y, Color color, int scale);

#endif
