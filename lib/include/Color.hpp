#ifndef _COLOR_HPP
#define _COLOR_HPP

#include <stdint.h>

typedef struct
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} Color;

#endif
