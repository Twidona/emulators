cmake_minimum_required(VERSION 2.8)
project(EmuLib)

set(LIBRARY_OUTPUT_PATH ${CMAKE_HOME_DIRECTORY}/bin)

find_package(SDL REQUIRED)

include_directories(include/)

file(
	GLOB
	sources
	src/*
)

add_library(
	emulib
	SHARED
	${sources}
)

target_link_libraries(
	emulib
	${SDL_LIBRARY}
)
