#include <iostream>
#include <string>

#include <Ini.hpp>

using namespace std;

int main(int argc, char *argv[])
{
	Ini ini;
	string str;

	IniMap inimap;
	inimap["Colors"]["white"] = "255,255,255";
	inimap["Colors"]["black"] = "0,0,0";
	inimap["Display"]["multiple"] = "5";
	inimap["Dummy"]["dummy"] = "dummy";

	if(argc != 2)
	{
		cout << "Usage: " << argv[0] << " file.ini" << endl;
		return -1;
	}

	ini.load(argv[1]);

	//Exemple de récupération de valeur à partir d'une catégorie et d'une clé
	cout << "Entrez catégorie.clé : ";
	cin >> str;
	string cat, key;
	int pos = str.find_first_of('.');
	if(pos == string::npos)
	{
		cout << "Saisie incorrecte (. manquant)" << endl;
		return -1;
	}
	cat = str.substr(0, pos);
	key = str.substr(pos + 1, str.length() - pos);
	cout << "Valeur : " << ini.get(cat, key) << endl;


	ini.display();
	ini.add("Display", "dummy", "dummy");
	ini.load_default(inimap);

	ini.save("out.ini");

	return 0;
}
