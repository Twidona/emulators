#ifndef _NES_OPCODES_HPP
#define _NES_OPCODES_HPP

#include <2A03.hpp>

/*http://www.obelisk.demon.co.uk/6502/reference.html*/

typedef unsigned int (*opc_2a03)(P2A03 *cpu);
typedef void (*opc_2a03_am)(P2A03 *cpu);

/*
Function suffixes :
ABS = absolute
ABX = absolute indexed with X = $addr,X
ABY = absolute indexed with Y = $addr,Y
ACC = accumulator
IMM = immediate
ZP = zero page
ZPX = sero page indexed with X
INY = Indirect indexed with Y
IND = Indirect
*/

//Instructions
unsigned int opc_2a03_INC(P2A03 *cpu);
unsigned int opc_2a03_DEC(P2A03 *cpu);
unsigned int opc_2a03_LDA(P2A03 *cpu);
unsigned int opc_2a03_LDX(P2A03 *cpu);
unsigned int opc_2a03_LDY(P2A03 *cpu);
unsigned int opc_2a03_STA(P2A03 *cpu);
unsigned int opc_2a03_STX(P2A03 *cpu);
unsigned int opc_2a03_STY(P2A03 *cpu);
unsigned int opc_2a03_CMP(P2A03 *cpu);
unsigned int opc_2a03_CPX(P2A03 *cpu);
unsigned int opc_2a03_CPY(P2A03 *cpu);
unsigned int opc_2a03_SBC(P2A03 *cpu);
unsigned int opc_2a03_ADC(P2A03 *cpu);
unsigned int opc_2a03_LSR(P2A03 *cpu);
unsigned int opc_2a03_ASL(P2A03 *cpu);
unsigned int opc_2a03_ROR(P2A03 *cpu);
unsigned int opc_2a03_ROL(P2A03 *cpu);
unsigned int opc_2a03_EOR(P2A03 *cpu);
unsigned int opc_2a03_ORA(P2A03 *cpu);
unsigned int opc_2a03_AND(P2A03 *cpu);
unsigned int opc_2a03_BIT(P2A03 *cpu);
unsigned int opc_2a03_JMP(P2A03 *cpu);

unsigned int opc_2a03_PHP(P2A03 *cpu);
unsigned int opc_2a03_PLP(P2A03 *cpu);
unsigned int opc_2a03_PHA(P2A03 *cpu);
unsigned int opc_2a03_PLA(P2A03 *cpu);

unsigned int opc_2a03_BEQ(P2A03 *cpu);
unsigned int opc_2a03_BNE(P2A03 *cpu);
unsigned int opc_2a03_BPL(P2A03 *cpu);
unsigned int opc_2a03_BMI(P2A03 *cpu);
unsigned int opc_2a03_BVC(P2A03 *cpu);
unsigned int opc_2a03_BVS(P2A03 *cpu);
unsigned int opc_2a03_BCC(P2A03 *cpu);
unsigned int opc_2a03_BCS(P2A03 *cpu);

unsigned int opc_2a03_CLC(P2A03 *cpu);
unsigned int opc_2a03_SEC(P2A03 *cpu);
unsigned int opc_2a03_CLI(P2A03 *cpu);
unsigned int opc_2a03_SEI(P2A03 *cpu);
unsigned int opc_2a03_CLV(P2A03 *cpu);
unsigned int opc_2a03_CLD(P2A03 *cpu);
unsigned int opc_2a03_SED(P2A03 *cpu);

unsigned int opc_2a03_TXA(P2A03 *cpu);
unsigned int opc_2a03_TYA(P2A03 *cpu);
unsigned int opc_2a03_TXS(P2A03 *cpu);
unsigned int opc_2a03_TSX(P2A03 *cpu);
unsigned int opc_2a03_TAY(P2A03 *cpu);
unsigned int opc_2a03_TAX(P2A03 *cpu);

unsigned int opc_2a03_BRK(P2A03 *cpu);
unsigned int opc_2a03_JSR(P2A03 *cpu);
unsigned int opc_2a03_RTI(P2A03 *cpu);
unsigned int opc_2a03_RTS(P2A03 *cpu);

unsigned int opc_2a03_DEY(P2A03 *cpu);
unsigned int opc_2a03_INY(P2A03 *cpu);
unsigned int opc_2a03_DEX(P2A03 *cpu);
unsigned int opc_2a03_INX(P2A03 *cpu);

unsigned int opc_2a03_NOP(P2A03 *cpu);


//Adressing modes
void opc_2a03_am_IMP(P2A03 *cpu);
void opc_2a03_am_IND(P2A03 *cpu);
void opc_2a03_am_IMM(P2A03 *cpu);
void opc_2a03_am_REL(P2A03 *cpu);
void opc_2a03_am_NOP(P2A03 *cpu);
void opc_2a03_am_ACC(P2A03 *cpu);
void opc_2a03_am_ABS(P2A03 *cpu);
void opc_2a03_am_ABX(P2A03 *cpu);
void opc_2a03_am_ABY(P2A03 *cpu);
void opc_2a03_am_ZP(P2A03 *cpu);
void opc_2a03_am_ZPX(P2A03 *cpu);
void opc_2a03_am_ZPY(P2A03 *cpu);
void opc_2a03_am_INX(P2A03 *cpu);
void opc_2a03_am_INY(P2A03 *cpu);

#endif
