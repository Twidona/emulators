#ifndef _NES_2A03_HPP
#define _NES_2A03_HPP

#include <stdint.h>

#include <PPU.hpp>
#include <Mapper.hpp>
#include <Events.hpp>
#include <Ini.hpp>

class NES_PPU;

typedef struct
{
	uint8_t c:1;
	uint8_t z:1;
	uint8_t i:1;
	uint8_t d:1;
	uint8_t b:1;
	uint8_t unused:1;
	uint8_t v:1;
	uint8_t n:1;
} P_reg;

#define get_P() ((uint8_t) (P.c | P.z << 1 | P.i << 2 | P.d << 3 | P.b << 4 | P.unused << 5 | P.v << 6 | P.n << 7))

#define get_P_2() ((uint8_t) (cpu->P.c | cpu->P.z << 1 | cpu->P.i << 2 | cpu->P.d << 3 | cpu->P.b << 4 | cpu->P.unused << 5 | cpu->P.v << 6 | cpu->P.n << 7))

#define set_P(p_reg) cpu->P.c = p_reg & 1; \
						cpu->P.z = p_reg >> 1 & 1; \
						cpu->P.i = p_reg >> 2 & 1; \
						cpu->P.d = p_reg >> 3 & 1; \
						cpu->P.b = p_reg >> 4 & 1; \
						cpu->P.unused = p_reg >> 5 & 1; \
						cpu->P.v = p_reg >> 6 & 1; \
						cpu->P.n = p_reg >> 7;

class P2A03
{
	private:
		//Registers
		uint16_t PC;
		uint8_t A;
		uint8_t X;
		uint8_t Y;
		uint8_t S;
		P_reg P;

		uint8_t ram[2*1024];

		//Current C.P.U. cycle since the beginning of the scanline
		unsigned int cur_cycle;

		//Vars set by opc_2a03_am_ functions, used by opc_2a03_ functions
		NES_Adressing_Mode am;
		uint8_t oper;
		uint16_t addr;
		unsigned int page_crossed_cycle; //Supplementary cycle taken for some instructions when a page is crossed

		//Pointers to other N.E.S. components. Their memory is allocated by the NES class
		NES_Mapper *mapper;
		NES_PPU *ppu;

		//Controller related variables
		Standard_Controller controller1;
		Standard_Controller controller2;

	public:
		P2A03();
		~P2A03();

		void load_ini(Ini &ini);

		//Those functions can access the entire memory space of the NES (CPU and PPU and Mapper)
		uint8_t read_m(NES_Memory_Space space, uint16_t addr);
		void write_m(NES_Memory_Space space, uint16_t addr, uint8_t val);

		//Functions to set the different pointers to the other components
		void set_mapper(NES_Mapper *_mapper);
		void unset_mapper();
		void set_ppu(NES_PPU *_ppu);
		void unset_ppu();

		//http://wiki.nesdev.com/w/index.php/CPU_power_up_state
		//Set the memory and registers to their default state at reset or power-up
		void make_powerup_state();
		void make_reset_state();

		void start_interrupt(NES_Interrupt_Type interrupt);

		//Execute the next instruction and increment cur_cycle
		bool next_instruction();

		friend unsigned int opc_2a03_INC(P2A03 *cpu);
		friend unsigned int opc_2a03_DEC(P2A03 *cpu);
		friend unsigned int opc_2a03_LDA(P2A03 *cpu);
		friend unsigned int opc_2a03_LDX(P2A03 *cpu);
		friend unsigned int opc_2a03_LDY(P2A03 *cpu);
		friend unsigned int opc_2a03_STA(P2A03 *cpu);
		friend unsigned int opc_2a03_STX(P2A03 *cpu);
		friend unsigned int opc_2a03_STY(P2A03 *cpu);
		friend unsigned int opc_2a03_CMP(P2A03 *cpu);
		friend unsigned int opc_2a03_CPX(P2A03 *cpu);
		friend unsigned int opc_2a03_CPY(P2A03 *cpu);
		friend unsigned int opc_2a03_SBC(P2A03 *cpu);
		friend unsigned int opc_2a03_ADC(P2A03 *cpu);
		friend unsigned int opc_2a03_LSR(P2A03 *cpu);
		friend unsigned int opc_2a03_ASL(P2A03 *cpu);
		friend unsigned int opc_2a03_ROR(P2A03 *cpu);
		friend unsigned int opc_2a03_ROL(P2A03 *cpu);
		friend unsigned int opc_2a03_EOR(P2A03 *cpu);
		friend unsigned int opc_2a03_ORA(P2A03 *cpu);
		friend unsigned int opc_2a03_AND(P2A03 *cpu);
		friend unsigned int opc_2a03_BIT(P2A03 *cpu);
		friend unsigned int opc_2a03_JMP(P2A03 *cpu);
		friend unsigned int opc_2a03_PHP(P2A03 *cpu);
		friend unsigned int opc_2a03_PLP(P2A03 *cpu);
		friend unsigned int opc_2a03_PHA(P2A03 *cpu);
		friend unsigned int opc_2a03_PLA(P2A03 *cpu);
		friend unsigned int opc_2a03_BEQ(P2A03 *cpu);
		friend unsigned int opc_2a03_BNE(P2A03 *cpu);
		friend unsigned int opc_2a03_BPL(P2A03 *cpu);
		friend unsigned int opc_2a03_BMI(P2A03 *cpu);
		friend unsigned int opc_2a03_BVC(P2A03 *cpu);
		friend unsigned int opc_2a03_BVS(P2A03 *cpu);
		friend unsigned int opc_2a03_BCC(P2A03 *cpu);
		friend unsigned int opc_2a03_BCS(P2A03 *cpu);
		friend unsigned int opc_2a03_CLC(P2A03 *cpu);
		friend unsigned int opc_2a03_SEC(P2A03 *cpu);
		friend unsigned int opc_2a03_CLI(P2A03 *cpu);
		friend unsigned int opc_2a03_SEI(P2A03 *cpu);
		friend unsigned int opc_2a03_CLV(P2A03 *cpu);
		friend unsigned int opc_2a03_CLD(P2A03 *cpu);
		friend unsigned int opc_2a03_SED(P2A03 *cpu);
		friend unsigned int opc_2a03_TXA(P2A03 *cpu);
		friend unsigned int opc_2a03_TYA(P2A03 *cpu);
		friend unsigned int opc_2a03_TXS(P2A03 *cpu);
		friend unsigned int opc_2a03_TSX(P2A03 *cpu);
		friend unsigned int opc_2a03_TAY(P2A03 *cpu);
		friend unsigned int opc_2a03_TAX(P2A03 *cpu);
		friend unsigned int opc_2a03_BRK(P2A03 *cpu);
		friend unsigned int opc_2a03_JSR(P2A03 *cpu);
		friend unsigned int opc_2a03_RTI(P2A03 *cpu);
		friend unsigned int opc_2a03_RTS(P2A03 *cpu);
		friend unsigned int opc_2a03_DEY(P2A03 *cpu);
		friend unsigned int opc_2a03_INY(P2A03 *cpu);
		friend unsigned int opc_2a03_DEX(P2A03 *cpu);
		friend unsigned int opc_2a03_INX(P2A03 *cpu);
		friend unsigned int opc_2a03_NOP(P2A03 *cpu);

		friend void opc_2a03_am_IMP(P2A03 *cpu);
		friend void opc_2a03_am_IND(P2A03 *cpu);
		friend void opc_2a03_am_IMM(P2A03 *cpu);
		friend void opc_2a03_am_REL(P2A03 *cpu);
		friend void opc_2a03_am_NOP(P2A03 *cpu);
		friend void opc_2a03_am_ACC(P2A03 *cpu);
		friend void opc_2a03_am_ABS(P2A03 *cpu);
		friend void opc_2a03_am_ABX(P2A03 *cpu);
		friend void opc_2a03_am_ABY(P2A03 *cpu);
		friend void opc_2a03_am_ZP(P2A03 *cpu);
		friend void opc_2a03_am_ZPX(P2A03 *cpu);
		friend void opc_2a03_am_ZPY(P2A03 *cpu);
		friend void opc_2a03_am_INX(P2A03 *cpu);
		friend void opc_2a03_am_INY(P2A03 *cpu);
};

#endif
