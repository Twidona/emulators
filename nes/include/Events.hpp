#ifndef _NES_EVENTS_HPP
#define _NES_EVENTS_HPP

#include <SDL/SDL.h>

#include <stdint.h>

#include <Defines.hpp>
#include <Ini.hpp>

class USB_Controller;

class Events
{
	public:
		Events();
		void init_controllers();
		~Events();
		void poll();

		bool is_quitev();

		bool get_button_state(unsigned int controller_nb, unsigned int button_nb);
		int get_axis_value(unsigned int controller_nb, unsigned int axis_nb);

		bool keys[SDLK_LAST];

	private:
		bool quitev;
		USB_Controller **controllers;
};

class USB_Controller
{
	friend class Events;

	public:
		USB_Controller(int ctrl_nb);
		~USB_Controller();

		bool get_button_state(unsigned int button_nb);
		int get_axis_value(unsigned int axis_nb);

	private:
		void set_button_state(unsigned int button_nb, bool state);
		void set_axis_value(unsigned int axis_nb, int value);

		SDL_Joystick *controller;

		unsigned int buttons_nb;
		unsigned int axes_nb;
		unsigned int hats_nb;

		bool *button_state;
		int *axis_value;
};

class Standard_Controller
{
	public:
		Standard_Controller();
		~Standard_Controller();

		void load_ini(Ini &ini, std::string cat);

		unsigned int read_next();
		void write_polling(bool new_polling);

	private:
		void update(Events &events);

		unsigned int state[STD_CTRL_BTNS_NB]; //0 = not pressed, 1 = pressed
		int state_keys[STD_CTRL_BTNS_NB];

		unsigned int controller_nb;
		Control_Type control_type[STD_CTRL_BTNS_NB];
		int control_value[STD_CTRL_BTNS_NB];
		int axis_sign[STD_CTRL_BTNS_NB]; //If the key use an axis, to know if the axis value must be positive of negative

		unsigned int n; //Indicate the index of the next value to be read in state

		bool polling; //If the controller's state is reloading (value written to $4016)
};


#endif
