#ifndef _NES_ROM_HPP
#define _NES_ROM_HPP

#include <Error.hpp>

#include <string>
#include <stdint.h>

class NES_ROM;

class NES_ROM_Info
{
	friend class NES_ROM;

	private:
		uint8_t prgrom_nb; //16kio * prgrom_nb
		uint8_t chrrom_nb; //8kio * chrrom_nb

		uint8_t mapper_nb; //Number of the mapper according to the iNes file format

		int mirroring; //Horizontal, vertical or 4 screens

		bool is_chrram; //if chrrom_nb == 0
		bool is_sram_battery_backed; //SRAM = Save RAM, additionnal battery backed RAM from $6000 to $7FFF
		bool is_trainer; //512 bytes, from $7000 to $71FF (Trainer = patch to add cheats, … = extra 512 bytes PRG-ROM) http://wiki.nesdev.com/w/index.php/Glossary#T

		//TODO : add support for PlayChoice INST-ROM and PROM
		bool is_vs_unisys; //The game is for the VS Unisystem, machine with a hardware similar to NES's
		bool is_playchoice; //The game is for the PlayChoice-10

	public:
		unsigned int get_prgrom_nb() const;
		unsigned int get_chrrom_nb() const;

		unsigned int get_mapper_nb() const;
		std::string get_mapper_string() const;

		int get_mirroring() const;
		std::string get_mirroring_string() const;

		bool get_is_sram_battery_backed() const;
		bool get_is_trainer() const;

		bool get_is_vs_unisys() const;
		bool get_is_playchoice() const;
};

//TODO : NES 2.0 header
//TODO : check only zeroes on other bytes 
class NES_ROM
{
	private:
		bool rom_loaded; //true if a rom is loaded

		NES_ROM_Info info;

		uint8_t *prgrom;
		uint8_t *chrrom;

	public:
		NES_ROM();
		NES_ROM(std::string path);
		NES_ROM(const char *path);
		~NES_ROM();

		void load(const char *path);
		void unload();

		bool is_loaded();

		const uint8_t* get_prgrom();
		const uint8_t* get_chrrom();

		NES_ROM_Info get_rom_info();
};

#endif
