#ifndef _NES_PPU_HPP
#define _NES_PPU_HPP

#include <stdint.h>

#include <SDL/SDL.h>

#include <2A03.hpp>
#include <Mapper.hpp>
#include <Defines.hpp>
#include <Ini.hpp>

class P2A03;

//TODO (tout plein de trucs)
//DMA
//Désactivation de la NMI (registre)
//Amélioration de la précision (= moment du changement du is_vblank)
//NMI
//temps d'inactivité du PPU
//quand on lit $2002, renvoie la valeur *précédente* de NMI_Occured
//find a better way to start the NMI interrupt (probably launch it from the PPU)
//Check byte order when pushing PC (JSR/interrupts)
//Check SBC behavior
//Check ROL_ZP behavior

class NES_PPU
{
	private:
		NES_Mapper *mapper;
		P2A03 *cpu;

		SDL_Surface *screen;

		uint8_t SPR_RAM[0x100];
		uint8_t SPR_RAM_2[8 * 4]; //SPR-RAM for the 8 sprites to be printed on the next scanline

		Color pal[0x40];

		//PPU memory's Name tables (+ attribute tables)
		uint8_t *real_nt1;
		uint8_t *real_nt2;

		//Pointer to the other name tables, forming the 4 NTs' screen
		uint8_t *nt1;
		uint8_t *nt2;
		uint8_t *nt3;
		uint8_t *nt4;

		uint8_t bg_pal[0x10];
		uint8_t spr_pal[0x10];

		uint8_t reg_2000;
		uint8_t reg_2001;
		uint8_t reg_2003;
		uint16_t reg_2006;

		NES_Mirroring mirroring;

		uint8_t VRAM_latch;

		uint8_t x_scroll;
		uint8_t y_scroll;

		uint8_t buffer; //When reading PPU memory

		unsigned int cur_scanline;

		bool is_vblank;
		bool is_vblank_2002;
		uint8_t sprite_0_hit;
		uint8_t sprite_overflow;

		int multiple;

	public:
		NES_PPU();
		~NES_PPU();

		uint8_t read_m(NES_Memory_Space space, uint16_t addr);
		void write_m(NES_Memory_Space space, uint16_t addr, uint8_t val);

		void set_mapper(NES_Mapper *_mapper);
		void unset_mapper();

		void set_cpu(P2A03 *_cpu);
		void unset_cpu();

		void set_screen(SDL_Surface *screen);

		//TODO
		void make_powerup_state();
		void make_reset_state();

		void switch_mirroring(NES_Mirroring _mirroring);
		
		unsigned int get_cur_scanline() const;
		void inc_cur_scanline();

		void render_scanline();

		void load_ini(Ini &ini);
};

void putPixel(SDL_Surface *surface, int x, int y, Color color, int multiple);

#endif
