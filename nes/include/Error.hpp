#ifndef _NES_ERROR_HPP
#define _NES_ERROR_HPP

#include <string>

//Errors
#define NES_NO_ERROR 0

//ROM loading errors
#define NES_ROM_LOAD_ERROR -1
#define NES_ROM_WRONG_FORMAT -2
#define NES_ROM_ALLOC_FAILURE -3

//Mappers errors
#define NES_MAPPER_ALLOC_FAILURE -4
#define NES_MAPPER_INCOMPATIBLE_INFO -5
#define NES_MAPPER_NOT_IMPLEMENTED -6
#define NES_MAPPER_BAD_READING -7 //Reading a space in memory one has normally no access to

//CPU errors
#define NES_CPU_BAD_READING -8

int NES_get_error();
std::string NES_get_error_string();

#endif
