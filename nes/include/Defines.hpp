#ifndef _NES_DEFINES_HPP
#define _NES_DEFINES_HPP

//Mappers
#define NES_MAPPER_NROM 0 //Always 32KiB PRG-ROM and 8KiB CHR-ROM (or 8KiB CHR-RAM for some demos ? = how do we know if we can let the program modify the CHR-ROM/CHR-RAM ?)
#define NES_MAPPER_MMC1 1 //With SxROM boards — to implement after mappers 0, 2 and 3 but before mapper 4
#define NES_MAPPER_UxROM 2 //And compatible mappers — seems simple to implement, to implement before any other mapper (with CNROM)
#define NES_MAPPER_CNROM 3 //And compatible mappers — seems simple to implement, to implement before any other mapper (with UxROM)
#define NES_MAPPER_MMC3 4 //And MMC6 — to implement after mappers 0, 1, 2 and 3

//Mirroring
enum NES_Mirroring {NES_MIRRORING_H = 0, NES_MIRRORING_V, NES_MIRRORING_4, NES_MIRRORING_1SA, NES_MIRRORING_1SB};

//Memory space
enum NES_Memory_Space {NES_MEMORY_PRG = 1, NES_MEMORY_CHR};

//Interrutps
enum NES_Interrupt_Type {NES_INTERRUPT_NMI = 1};

//Adressing modes
enum NES_Adressing_Mode {NES_AM_IMP = 1, NES_AM_INX, NES_AM_NOP, NES_AM_ZP, NES_AM_IMM, NES_AM_ACC, NES_AM_ABS, NES_AM_REL, NES_AM_ABY, NES_AM_ABX, NES_AM_INY, NES_AM_ZPX, NES_AM_IND, NES_AM_ZPY};

//Latches
#define LATCH_WRITE1 0
#define LATCH_WRITE2 1

//Standard Controller Buttons, ordered by read order
enum STD_CTRL_BTNS {STD_CTRL_A = 0, STD_CTRL_B, STD_CTRL_Select, STD_CTRL_Start, STD_CTRL_Up, STD_CTRL_Down, STD_CTRL_Left, STD_CTRL_Right, STD_CTRL_BTNS_NB};

enum Control_Type{Control_Kbd, Control_Button, Control_Axis};

typedef struct
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} Color;

#endif
