#ifndef _NES_NES_HPP
#define _NES_NES_HPP

#include <SDL/SDL.h>

#include <2A03.hpp>
#include <Mapper.hpp>
#include <PPU.hpp>
#include <ROM.hpp>
#include <Events.hpp>
#include <Ini.hpp>

class NES
{
	private:
		//NES components. The NES manage their allocation : no component should free the others
		NES_Mapper *mapper;
		P2A03 *cpu;
		NES_PPU *ppu;

		//Information about the ROM loaded in the mapper
		NES_ROM_Info *rom_info;

		bool mapper_loaded;
		bool is_on; //to know if we have to start or to reset the NES

		void make_powerup_state();

	public:
		NES();
		~NES();

		void load(const char *path);
		void unload();

		const NES_ROM_Info* get_rom_info();

		void next_frame();
		void next_scanline();

		void set_screen(SDL_Surface *screen);

		void load_ini(Ini &ini);
};

#endif
