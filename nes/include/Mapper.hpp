#ifndef _NES_MAPPER_HPP
#define _NES_MAPPER_HPP

#include <string>
#include <ROM.hpp>
#include <Defines.hpp>

class NES_PPU;

//TODO: add mirroring to mapper class ? (Knowing that some mappers control mirroring)
class NES_Mapper
{
	public:
		//Those functions can only access the memory of the mapper (PRGROM/RAM, CHRROM/RAM, WRAM)
		virtual uint8_t read_m(NES_Memory_Space space, uint16_t addr) = 0;
		virtual void write_m(NES_Memory_Space space, uint16_t addr, uint8_t val) = 0;

		virtual std::string get_name() const = 0;

		//Check if the information of the header of the .nes file match the mapper caracteristics
		static bool check(NES_ROM &rom);
};

NES_Mapper *mapper_from_rom(NES_ROM &rom, NES_PPU *ppu);

//Mapper 0
class NES_Mapper_NROM : public NES_Mapper
{
	private:
		std::string name;

		uint8_t prgrom_nb;
		uint8_t *prgrom; //1 or 2 * 16KiB
		uint8_t *chrrom; //1 * 8KiB
		//uint8_t *prgram; for family basic ? (TODO)

	public:
		NES_Mapper_NROM(NES_ROM &rom);
		~NES_Mapper_NROM();

		uint8_t read_m(NES_Memory_Space space, uint16_t addr);
		void write_m(NES_Memory_Space space, uint16_t addr, uint8_t val);

		std::string get_name() const;

		static bool check(NES_ROM &rom);
};

//Mapper 1
class NES_Mapper_MMC1 : public NES_Mapper
{
	private:
		std::string name;

		uint8_t prgrom_nb;
		uint8_t *prgrom;

		uint8_t chrrom_nb; //0 for CHR-RAM
		uint8_t *chrrom;

		uint8_t *prgram;
		bool is_battery_backed;

		NES_Mirroring mirroring;

		uint8_t tmp_reg;
		uint8_t tmp_reg_bit; // 0 <= tmp_reg_bit <= 4

		uint8_t reg_8000;
		uint8_t reg_A000;
		uint8_t reg_C000;
		uint8_t reg_E000;

		NES_PPU *ppu;

	public:
		NES_Mapper_MMC1(NES_ROM &rom, NES_PPU *_ppu);
		~NES_Mapper_MMC1();

		uint8_t read_m(NES_Memory_Space space, uint16_t addr);
		void write_m(NES_Memory_Space space, uint16_t addr, uint8_t val);

		std::string get_name() const;

		static bool check(NES_ROM &rom);
};

//Mapper 2
/*
    PRG ROM size: 128 KB for UNROM, 256 KB for UOROM (DIP-28/32 Nintendo pinout)
    PRG ROM bank size: 16 KB
    PRG RAM: None
    CHR capacity: 8 KB RAM
    CHR bank size: Not bankswitched
    Nametable mirroring: Solder pads select vertical or horizontal mirroring
    Subject to bus conflicts: Yes 
*/
class NES_Mapper_UxROM : public NES_Mapper
{
	private:
		std::string name;

		uint8_t prgrom_nb;
		uint8_t *prgrom;
		uint8_t *chrram; //1 * 8KiB CHR-*RAM*

		uint8_t selected_bank;

	public:
		NES_Mapper_UxROM(NES_ROM &rom);
		~NES_Mapper_UxROM();

		uint8_t read_m(NES_Memory_Space space, uint16_t addr);
		void write_m(NES_Memory_Space space, uint16_t addr, uint8_t val);

		std::string get_name() const;

		static bool check(NES_ROM &rom);
};

//Mapper 3
/*
     PRG ROM size: 16 KB or 32 KB (DIP-28 standard pinout)
    PRG ROM bank size: Not bankswitched
    PRG RAM: None
    CHR capacity: 32 KB ROM (DIP-28 standard pinout)
    CHR bank size: 8 KB
    Nametable mirroring: Solder pads select vertical or horizontal mirroring
    Subject to bus conflicts: Yes 
*/
// Note: mapper 3 = CNROM *and compatibles*, compatibles = TODO
class NES_Mapper_CNROM : public NES_Mapper
{
	private:
		std::string name;

		uint8_t prgrom_nb;
		uint8_t *prgrom; //16 or 32 kiB PRG-ROM
		uint8_t *chrrom; //32 kiB CHR-ROM

		uint8_t selected_bank; //between 0 and 3 (note : some compatible boards may exceed this limit, TODO)

	public:
		NES_Mapper_CNROM(NES_ROM &rom);
		~NES_Mapper_CNROM();

		uint8_t read_m(NES_Memory_Space space, uint16_t addr);
		void write_m(NES_Memory_Space space, uint16_t addr, uint8_t val);

		std::string get_name() const;

		static bool check(NES_ROM &rom);
};

#endif
