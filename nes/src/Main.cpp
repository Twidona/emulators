#include <ROM.hpp>
#include <Mapper.hpp>
#include <NES.hpp>
#include <PPU.hpp>
#include <Error.hpp>
#include <Ini.hpp>

#include <iostream>
#include <cstdio>
#include <cstdlib>

#include <SDL/SDL.h>

#ifdef USE_PORTAUDIO
	#include <portaudio.h>
#endif

using namespace std;

extern int NES_error;
extern Events events;

int main(int argc, char *argv[])
{
	NES nes;
	SDL_Surface *screen = NULL;
	const NES_ROM_Info *info = NULL;
	Ini ini;
	IniMap inimap;

	#ifdef USE_PORTAUDIO
	PaError err;
	#endif

	string home = getenv("HOME");
	if(!home.empty())
	{
		string path = home + "/.config/emulators/nes.ini";
		ini.load(path);
	}

	inimap["Ctrl1"]["A"] = "119";
	inimap["Ctrl1"]["B"] = "120";
	inimap["Ctrl1"]["Start"] = "13";
	inimap["Ctrl1"]["Select"] = "32";
	inimap["Ctrl1"]["Left"] = "276";
	inimap["Ctrl1"]["Right"] = "275";
	inimap["Ctrl1"]["Up"] = "273";
	inimap["Ctrl1"]["Down"] = "274";
	inimap["General"]["DisplayMultiple"] = "1";
	ini.load_default(inimap);

	nes.load_ini(ini);

	if(argc != 2)
	{
		cout << "Usage: " << argv[0] << " rom.nes" << endl;
		return -1;
	}

	#ifdef USE_PORTAUDIO
	err = Pa_Initialize();
	if(err != paNoError)
	{
			cerr << "Error initializing Port Audio: " << Pa_GetErrorText(err) << endl;
			return -1;
	}
	#endif

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) == -1)
	{
		fprintf(stderr, "Error initializing SDL: %s\n", SDL_GetError());
		#ifdef USE_PORTAUDIO
		Pa_Terminate();
		#endif
		return -1;
	}

	int multiple = atoi(ini.get("General", "DisplayMultiple").c_str());
	if(multiple < 1)
		multiple = 1;
	else if(multiple > 4)
		multiple = 4;
	screen = SDL_SetVideoMode(256 * multiple, 240 * multiple, 32, SDL_SWSURFACE);
	if(!screen)
	{
		fprintf(stderr, "Error opening window: %s\n", SDL_GetError());
		#ifdef USE_PORTAUDIO
		Pa_Terminate();
		#endif
		SDL_Quit();
		return -1;
	}
	SDL_WM_SetCaption("NES Emulator", NULL);
	SDL_JoystickEventState(SDL_ENABLE);

	events.init_controllers();

	nes.load(argv[1]);

	if(NES_get_error() != NES_NO_ERROR)
	{
		cout << "Error loading ROM: " << NES_get_error_string() << endl;
		#ifdef USE_PORTAUDIO
		Pa_Terminate();
		#endif
		SDL_Quit();
		return -1;
	}

	nes.set_screen(screen);

	info = nes.get_rom_info();

	cout << argv[1] << " loaded with mapper " << info->get_mapper_string() << endl << endl;

	cout << argv[1] << " ROM information:" << endl;
	cout << "—————" << endl;
	cout << "PRG-ROM: " << info->get_prgrom_nb() * 16 << "kio" << endl;
	cout << "CHR-ROM: " << info->get_chrrom_nb() * 8 << "kio" << endl;
	cout << "Mapper: " << info->get_mapper_nb() << " (" << info->get_mapper_string() << ")" << endl;
	cout << "Mirroring: " << info->get_mirroring_string() << endl;
	cout << "Battery Backed SRAM: " << (info->get_is_sram_battery_backed() ? "Yes" : "No") << endl;
	cout << "Trainer: " << (info->get_is_trainer() ? "Yes" : "No") << endl;
	cout << "VS Unisystem: " << (info->get_is_vs_unisys() ? "Yes" : "No") << endl;
	cout << "PlayChoice-10: " << (info->get_is_playchoice() ? "Yes" : "No") << endl;

	Uint32 cur_time = 0, last_time = 0;
	cur_time = last_time = SDL_GetTicks();
	for(;;)
	{
		//Next frame generation and rendering
		nes.next_frame();
		SDL_Flip(screen);

		//We limit the framerate to approximately 60 F.P.S.
		cur_time = SDL_GetTicks();
		while(cur_time - last_time < 17)
		{
			cur_time = SDL_GetTicks();
		}
		last_time = cur_time;

		//We update the events and quit if needed
		events.poll();
		if(events.is_quitev())
			break;
	}

	SDL_Quit();
	#ifdef USE_PORTAUDIO
	Pa_Terminate();
	#endif

	return 0;
}
