#include <Mapper.hpp>
#include <PPU.hpp>

#include <cstdlib>
#include <cstdio>
#include <cstring> //for memcpy
#include <string>
#include <iostream>

using namespace std;

extern int NES_error;

bool NES_Mapper::check(NES_ROM &rom)
{
	return false;
}

NES_Mapper_NROM::NES_Mapper_NROM(NES_ROM &rom) : name("NROM"), prgrom(NULL), chrrom(NULL)
{
	NES_ROM_Info info = rom.get_rom_info();

	const uint8_t *tmp_prg = rom.get_prgrom(), *tmp_chr = rom.get_chrrom();

	chrrom = (uint8_t*) malloc(1024 * 8);
	if(chrrom)
		memcpy(chrrom, tmp_chr, 1024 * 8);
	else
	{
		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}


	prgrom = (uint8_t*) malloc(1024 * 16 * info.get_prgrom_nb());
	if(prgrom)
		memcpy(prgrom, tmp_prg, 1024 * 16 * info.get_prgrom_nb());
	else
	{
		free(chrrom);
		chrrom = NULL;
		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}

	prgrom_nb = info.get_prgrom_nb();
	NES_error = NES_NO_ERROR;
}

NES_Mapper_NROM::~NES_Mapper_NROM()
{
	if(prgrom)
	{
		free(prgrom);
		prgrom = NULL;
	}

	if(chrrom)
	{
		free(chrrom);
		chrrom = NULL;
	}
}

uint8_t NES_Mapper_NROM::read_m(NES_Memory_Space space, uint16_t addr)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0x8000)
		{
			return prgrom[addr - 0x8000 - (2 - prgrom_nb) * 0x4000];
		}
		else //TODO : family basic RAM
		{
			return 0;
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		return chrrom[addr];
	}

	return 0;
}

void NES_Mapper_NROM::write_m(NES_Memory_Space space, uint16_t addr, uint8_t val)
{
	//TODO: allow writing in Family Basic RAM
	//Empty function: impossible to write anything in CHRROM/PRGROM

}

string NES_Mapper_NROM::get_name() const
{
	return name;
}

bool NES_Mapper_NROM::check(NES_ROM &rom)
{
	NES_ROM_Info info = rom.get_rom_info();

	if(info.get_mapper_nb() != NES_MAPPER_NROM)
		return false;

	if(info.get_prgrom_nb() != 1 && info.get_prgrom_nb() != 2) //16 ou 32kiB
		return false;

	if(info.get_chrrom_nb() != 1)
		return false;

	if(info.get_is_vs_unisys() || info.get_is_playchoice())
		return false;

	return true;
}

NES_Mapper_MMC1::NES_Mapper_MMC1(NES_ROM &rom, NES_PPU *_ppu) : name("MMC1"), prgrom(NULL), chrrom(NULL)
{
	NES_ROM_Info info = rom.get_rom_info();

	const uint8_t *tmp_prg = rom.get_prgrom(), *tmp_chr = rom.get_chrrom();

	if(info.get_chrrom_nb() == 0)
		chrrom = (uint8_t*) malloc(1024 * 8); //Well, its CHR-RAM, not ROM
	else
		chrrom = (uint8_t*) malloc(1024 * 8 * info.get_chrrom_nb());

	if(chrrom)
	{
		if(info.get_chrrom_nb() != 0)
			memcpy(chrrom, tmp_chr, 1024 * 8 * info.get_chrrom_nb());
		else
			memset(chrrom, 0, 8 * 1024);
	}
	else
	{
		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}


	prgrom = (uint8_t*) malloc(1024 * 16 * info.get_prgrom_nb());
	if(prgrom)
		memcpy(prgrom, tmp_prg, 1024 * 16 * info.get_prgrom_nb());
	else
	{
		free(chrrom);
		chrrom = NULL;
		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}

	prgram = (uint8_t*) malloc(1024 * 8);
	if(prgram)
		memset(chrrom, 0, 8 * 1024);
	else
	{
		free(chrrom);
		chrrom = NULL;

		free(prgrom);
		prgrom = NULL;

		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}

	is_battery_backed = info.get_is_sram_battery_backed();

	prgrom_nb = info.get_prgrom_nb();
	chrrom_nb = info.get_chrrom_nb();

	tmp_reg = tmp_reg_bit = reg_A000 = reg_C000 = 0;
	reg_E000 = 0x0;
	reg_8000 = 12;

	ppu = _ppu;
	ppu->switch_mirroring(mirroring = NES_MIRRORING_1SA);

	NES_error = NES_NO_ERROR;
}

NES_Mapper_MMC1::~NES_Mapper_MMC1()
{
	if(prgrom)
	{
		free(prgrom);
		prgrom = NULL;
	}

	if(chrrom)
	{
		free(chrrom);
		chrrom = NULL;
	}

	if(prgram)
	{
		free(prgram);
		prgram = NULL;
	}
}

uint8_t NES_Mapper_MMC1::read_m(NES_Memory_Space space, uint16_t addr)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0x8000)
		{
			if(reg_8000 >> 3 & 1)
			{
				if(reg_8000 >> 2 & 1)
				{
					if(addr < 0xC000)
					{
						return prgrom[(reg_E000 & (prgrom_nb - 1)) * 16 * 1024 + addr - 0x8000];
					}
					else
					{
						return prgrom[(prgrom_nb - 1) * 16 * 1024 + addr - 0xC000];
					}
				}
				else
				{
					if(addr < 0xC000)
					{
						return prgrom[addr - 0x8000];
					}
					else
					{
						return prgrom[(reg_E000 & (prgrom_nb - 1)) * 16 * 1024 + addr - 0xC000];
					}
				}				
			}
			else
			{
				return prgrom[((reg_E000 >> 1) & (prgrom_nb / 2 - 1)) * 32 * 1024 + addr - 0x8000];
			}
		}
		else if(addr >= 0x6000)
		{
			return prgram[addr - 0x6000];
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		if(chrrom_nb != 0)
		{
			if(reg_8000 >> 4)
			{
				if(addr < 0x1000)
				{
					return chrrom[4 * 1024 * (reg_A000 & (chrrom_nb * 2 - 1)) + addr];
				}
				else
				{
					return chrrom[4 * 1024 * (reg_C000 & (chrrom_nb * 2 - 1)) + addr - 0x1000];
				}
			}
			else
			{
				return chrrom[8 * 1024 * ((reg_A000 >> 1) & (chrrom_nb - 1)) + addr];
			}
		}
		else
		{
			return chrrom[addr];
		}
	}

	return 0;
}

void NES_Mapper_MMC1::write_m(NES_Memory_Space space, uint16_t addr, uint8_t val)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0x8000)
		{
			if(val >> 7)
			{
				tmp_reg = tmp_reg_bit = 0;
				reg_8000 |= 0x0C;
				mirroring = reg_8000 & 3 == 0 ? NES_MIRRORING_1SA : tmp_reg & 3 == 1 ? NES_MIRRORING_1SB : tmp_reg & 3 == 2 ? NES_MIRRORING_V : NES_MIRRORING_H;
				ppu->switch_mirroring(mirroring);
			}
			else
			{
				tmp_reg |= (val & 1) << tmp_reg_bit;
				tmp_reg_bit++;

				if(tmp_reg_bit == 5)
				{
					if(addr >= 0xE000)
						reg_E000 = tmp_reg;
					else if(addr >= 0xC000)
						reg_C000 = tmp_reg;
					else if(addr >= 0xA000)
						reg_A000 = tmp_reg;
					else
					{
						reg_8000 = tmp_reg;
						mirroring = tmp_reg & 3 == 0 ? NES_MIRRORING_1SA : tmp_reg & 3 == 1 ? NES_MIRRORING_1SB : tmp_reg & 3 == 2 ? NES_MIRRORING_V : NES_MIRRORING_H;
						ppu->switch_mirroring(mirroring);
					}

					tmp_reg_bit = tmp_reg = 0;
				}
			}
		}
		else if(addr >= 0x6000)
		{
			prgram[addr - 0x6000] = val;
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		if(chrrom_nb == 0)
			chrrom[addr] = val;
	}
}

string NES_Mapper_MMC1::get_name() const
{
	return name;
}

bool NES_Mapper_MMC1::check(NES_ROM &rom)
{
	return true;
}

NES_Mapper_UxROM::NES_Mapper_UxROM(NES_ROM &rom) : name("UxROM"), prgrom(NULL), chrram(NULL)
{
	NES_ROM_Info info = rom.get_rom_info();
	const uint8_t *tmp_prg = rom.get_prgrom();

	chrram = (uint8_t*) malloc(1024 * 8);
	if(chrram)
		memset(chrram, 0, 8 * 1024);
	else
	{
		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}


	prgrom = (uint8_t*) malloc(1024 * 16 * info.get_prgrom_nb());
	if(prgrom)
		memcpy(prgrom, tmp_prg, 1024 * 16 * info.get_prgrom_nb());
	else
	{
		free(chrram);
		chrram = NULL;
		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}

	prgrom_nb = info.get_prgrom_nb();
	NES_error = NES_NO_ERROR;
}

NES_Mapper_UxROM::~NES_Mapper_UxROM()
{
	if(prgrom)
	{
		free(prgrom);
		prgrom = NULL;
	}

	if(chrram)
	{
		free(chrram);
		chrram = NULL;
	}
}

uint8_t NES_Mapper_UxROM::read_m(NES_Memory_Space space, uint16_t addr)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0xC000)
		{
			NES_error = NES_NO_ERROR;
			return prgrom[0x4000 * (prgrom_nb - 1) + (addr - 0xC000)];
		}
		else if(addr >= 0x8000)
		{
			NES_error = NES_NO_ERROR;
			return prgrom[addr - 0x8000 + selected_bank * 0x4000];
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		return chrram[addr];
	}

	return 0;
}

void NES_Mapper_UxROM::write_m(NES_Memory_Space space, uint16_t addr, uint8_t val)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0x8000)
		{
			selected_bank = val & (prgrom_nb - 1);
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		chrram[addr] = val;
	}
}

string NES_Mapper_UxROM::get_name() const
{
	return name;
}

bool NES_Mapper_UxROM::check(NES_ROM &rom)
{
	NES_ROM_Info info = rom.get_rom_info();

	if(info.get_mapper_nb() != NES_MAPPER_UxROM)
		return false;

	if(info.get_prgrom_nb() != 8 && info.get_prgrom_nb() != 16) //128 or 256kiB
		return false;

	if(info.get_chrrom_nb() != 0)
		return false;

	if(info.get_is_vs_unisys() || info.get_is_playchoice())
		return false;

	return true;
}

NES_Mapper_CNROM::NES_Mapper_CNROM(NES_ROM &rom) : name("CNROM"), prgrom(NULL), chrrom(NULL), selected_bank(0)
{
	NES_ROM_Info info = rom.get_rom_info();
	const uint8_t *tmp_prg = rom.get_prgrom();
	const uint8_t *tmp_chr = rom.get_chrrom();

	chrrom = (uint8_t*) malloc(1024 * 8 * 4);
	if(chrrom)
		memcpy(chrrom, tmp_chr, 1024 * 8 * 4);
	else
	{
		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}


	prgrom = (uint8_t*) malloc(1024 * 16 * info.get_prgrom_nb());
	if(prgrom)
		memcpy(prgrom, tmp_prg, 1024 * 16 * info.get_prgrom_nb());
	else
	{
		free(chrrom);
		chrrom = NULL;
		NES_error = NES_MAPPER_ALLOC_FAILURE;
		return;
	}

	prgrom_nb = info.get_prgrom_nb();
	NES_error = NES_NO_ERROR;
}

NES_Mapper_CNROM::~NES_Mapper_CNROM()
{
	if(prgrom)
	{
		free(prgrom);
		prgrom = NULL;
	}

	if(chrrom)
	{
		free(chrrom);
		chrrom = NULL;
	}
}

uint8_t NES_Mapper_CNROM::read_m(NES_Memory_Space space, uint16_t addr)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0x8000)
		{
			return prgrom[addr - 0x8000 - (2 - prgrom_nb) * 0x4000];
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		return chrrom[addr + 8 * 1024 * selected_bank];
	}

	return 0;
}

void NES_Mapper_CNROM::write_m(NES_Memory_Space space, uint16_t addr, uint8_t val)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0x8000)
		{
			selected_bank = val & 3;
		}
	}
}

string NES_Mapper_CNROM::get_name() const
{
	return name;
}

bool NES_Mapper_CNROM::check(NES_ROM &rom)
{
	NES_ROM_Info info = rom.get_rom_info();

	if(info.get_mapper_nb() != NES_MAPPER_CNROM)
		return false;

	if(info.get_prgrom_nb() != 1 && info.get_prgrom_nb() != 2)
		return false;

	if(info.get_chrrom_nb() != 4)
		return false;

	if(info.get_is_vs_unisys() || info.get_is_playchoice())
		return false;

	return true;
}

NES_Mapper *mapper_from_rom(NES_ROM &rom, NES_PPU *ppu)
{
	NES_ROM_Info info = rom.get_rom_info();

	switch(info.get_mapper_nb())
	{
		case NES_MAPPER_NROM:
			if(NES_Mapper_NROM::check(rom))
			{
				NES_error = NES_NO_ERROR;
				return new NES_Mapper_NROM(rom);
			}
			else
			{
				NES_error = NES_MAPPER_INCOMPATIBLE_INFO;
				return NULL;
			}
		case NES_MAPPER_MMC1:
			if(NES_Mapper_MMC1::check(rom))
			{
				NES_error = NES_NO_ERROR;
				return new NES_Mapper_MMC1(rom, ppu);
			}
			else
			{
				NES_error = NES_MAPPER_INCOMPATIBLE_INFO;
				return NULL;
			}
		case NES_MAPPER_UxROM:
			if(NES_Mapper_UxROM::check(rom))
			{
				NES_error = NES_NO_ERROR;
				return new NES_Mapper_UxROM(rom);
			}
			else
			{
				NES_error = NES_MAPPER_INCOMPATIBLE_INFO;
				return NULL;
			}
		case NES_MAPPER_CNROM:
			if(NES_Mapper_CNROM::check(rom))
			{
				NES_error = NES_NO_ERROR;
				return new NES_Mapper_CNROM(rom);
			}
			else
			{
				NES_error = NES_MAPPER_INCOMPATIBLE_INFO;
				return NULL;
			}
		default:
			NES_error = NES_MAPPER_NOT_IMPLEMENTED;
			return NULL;
	}
}
