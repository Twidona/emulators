#include <cstdlib>
#include <string>

#include <SDL/SDL.h>

#include <Events.hpp>
#include <Ini.hpp>

Events events;

using namespace std;

Events::Events() : quitev(false)
{
	for(int i = 0 ; i < SDLK_LAST ; i++)
		keys[i] = false;
}

void Events::init_controllers()
{
	if(SDL_NumJoysticks() >= 1)
	{
		controllers = new USB_Controller*[SDL_NumJoysticks()];

		for(int i = 0 ; i < SDL_NumJoysticks() ; i++)
		{
			controllers[i] = new USB_Controller(i);
		}
	}
	else
	{
		controllers = NULL;
	}
}

Events::~Events()
{

}

void Events::poll()
{
	SDL_Event event;

	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_QUIT:
				quitev = true;
				break;

			case SDL_KEYDOWN:
				keys[event.key.keysym.sym] = true;
				break;

			case SDL_KEYUP:
				keys[event.key.keysym.sym] = false;
				break;

			case SDL_JOYBUTTONDOWN:
				controllers[event.jbutton.which]->set_button_state(event.jbutton.button, true);
				break;

			case SDL_JOYBUTTONUP:
				controllers[event.jbutton.which]->set_button_state(event.jbutton.button, false);
				break;

			case SDL_JOYAXISMOTION:
				controllers[event.jaxis.which]->set_axis_value(event.jaxis.axis, event.jaxis.value);
				break;
		}
	}
}

bool Events::is_quitev()
{
	return quitev;
}

bool Events::get_button_state(unsigned int controller_nb, unsigned int button_nb)
{
	if(controller_nb < SDL_NumJoysticks())
	{
		controllers[controller_nb]->get_button_state(button_nb);
	}
	else
	{
		return false;
	}
}

int Events::get_axis_value(unsigned int controller_nb, unsigned int axis_nb)
{
	if(controller_nb < SDL_NumJoysticks())
	{
		controllers[controller_nb]->get_axis_value(axis_nb);
	}
	else
	{
		return 0;
	}
}

USB_Controller::USB_Controller(int ctrl_nb)
{
	controller = SDL_JoystickOpen(ctrl_nb);

	buttons_nb = SDL_JoystickNumButtons(controller);
	axes_nb = SDL_JoystickNumAxes(controller);
	hats_nb = SDL_JoystickNumHats(controller);

	button_state = new bool[buttons_nb];
	axis_value = new int[axes_nb];

	for(int i = 0 ; i < buttons_nb ; i++)
		button_state[i] = false;

	for(int i = 0 ; i  < axes_nb ; i++)
		axis_value[i] = 0;
}

USB_Controller::~USB_Controller()
{
	SDL_JoystickClose(controller);

	delete[] button_state;
	delete[] axis_value;
}

void USB_Controller::set_button_state(unsigned int button_nb, bool state)
{
	if(button_nb >= buttons_nb)
		button_nb = buttons_nb - 1;

	button_state[button_nb] = state;
}

void USB_Controller::set_axis_value(unsigned int axis_nb, int value)
{
	if(axis_nb >= axes_nb)
		axis_nb = axes_nb - 1;

	axis_value[axis_nb] = value;
}

bool USB_Controller::get_button_state(unsigned int button_nb)
{
	if(button_nb >= buttons_nb)
		button_nb = buttons_nb - 1;

	return button_state[button_nb];
}

int USB_Controller::get_axis_value(unsigned int axis_nb)
{
	if(axis_nb >= axes_nb)
		axis_nb = axes_nb - 1;

	return axis_value[axis_nb];
}

Standard_Controller::Standard_Controller() : n(0), polling(false)
{
	for(int i = 0 ; i < STD_CTRL_BTNS_NB ; i++)
	{
		state[i] = 1;	
		state_keys[i] = 0;
	}
}

void Standard_Controller::load_ini(Ini &ini, std::string cat)
{
	string cur_string = "", button_string = "";

	controller_nb = atoi(ini.get(cat, "CtrlNb").c_str());

	for(int i = 0 ; i < STD_CTRL_BTNS_NB ; i++)
	{
		switch(i)
		{
			case STD_CTRL_A: button_string = "A"; break;
			case STD_CTRL_B: button_string = "B"; break;
			case STD_CTRL_Select: button_string = "Select"; break;
			case STD_CTRL_Start: button_string = "Start"; break;
			case STD_CTRL_Up: button_string = "Up"; break;
			case STD_CTRL_Down: button_string = "Down"; break;
			case STD_CTRL_Left: button_string = "Left"; break;
			case STD_CTRL_Right: button_string = "Right"; break;
		}

		cur_string = ini.get(cat, button_string);

		if(cur_string[0] == 'b')
		{
			control_type[i] = Control_Button;
			control_value[i] = atoi(cur_string.substr(1, cur_string.length() - 1).c_str());
		}
		else if(cur_string[0] == 'a')
		{
			control_type[i] = Control_Axis;
			axis_sign[i] = cur_string[1] == '-' ? -1 : 1;
			control_value[i] = atoi(cur_string.substr(2, cur_string.length() - 2).c_str());
		}
		else
		{
			control_type[i] = Control_Kbd;
			control_value[i] = atoi(cur_string.substr(0, cur_string.length()).c_str());
		}
	}
}

Standard_Controller::~Standard_Controller()
{

}

void Standard_Controller::update(Events &events)
{
	for(int i = 0 ; i < STD_CTRL_BTNS_NB ; i++)
	{
		switch(control_type[i])
		{
			case Control_Kbd:
				state[i] = events.keys[control_value[i]];
				break;

			case Control_Button:
				state[i] = events.get_button_state(controller_nb, control_value[i]);
				break;

			case Control_Axis:
				if(axis_sign[i] == 1)
					state[i] = events.get_axis_value(controller_nb, control_value[i]) >= 10000;
				else
					state[i] = events.get_axis_value(controller_nb, control_value[i]) <= -10000;
				break;
		}
	}
}

unsigned int Standard_Controller::read_next()
{
	if(polling == true)
		return 1;

	if(n < STD_CTRL_BTNS_NB)
	{
		return state[n++];
	}
	else
		return 1;
}

void Standard_Controller::write_polling(bool new_polling)
{
	if(polling != new_polling && new_polling == false)
	{
		n = 0;
		update(events);
	}

	polling = new_polling;
}
