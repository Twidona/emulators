#include <ROM.hpp>
#include <Defines.hpp>

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cstring>
#include <stdint.h>

using namespace std;

extern int NES_error;

NES_ROM::NES_ROM() : rom_loaded(false)
{
	NES_error = NES_NO_ERROR;
}

NES_ROM::NES_ROM(string path) : rom_loaded(false)
{
	NES_error = NES_NO_ERROR;
	load(path.c_str());
}

NES_ROM::NES_ROM(const char *path) : rom_loaded(false)
{
	NES_error = NES_NO_ERROR;
	load(path);
}

void NES_ROM::load(const char *path)
{
	FILE *romfile = NULL;
	char file_header[5] = {0};
	uint8_t flags1 = 0, flags2 = 0;

	//We unload the previous ROM if one was loaded
	if(rom_loaded)
	{
		unload();
	}

	romfile = fopen(path, "rb");
	if(!romfile)
	{
		NES_error = NES_ROM_LOAD_ERROR;
		return;
	}

	fread(file_header, 1, 4, romfile);
	if(strcmp(file_header, "NES\x1A") != 0)
	{
		NES_error = NES_ROM_WRONG_FORMAT;
		fclose(romfile);
		return;
	}

	fread(&(info.prgrom_nb), 1, 1, romfile);
	if(info.prgrom_nb == 0) //TODO check what does prgrom_nb == 0
	{
		NES_error = NES_ROM_WRONG_FORMAT;
		fclose(romfile);
		return;
	}

	fread(&(info.chrrom_nb), 1, 1, romfile);
	if(info.chrrom_nb == 0)
		info.is_chrram = true;

	prgrom = (uint8_t*) malloc(sizeof(uint8_t) * info.prgrom_nb * 16 * 1024);
	chrrom = info.is_chrram ? NULL : (uint8_t*) malloc(sizeof(uint8_t) * info.chrrom_nb * 8 * 1024);

	if(!prgrom || (!chrrom && !info.is_chrram))
	{
		if(prgrom)
			free(prgrom);
		if(chrrom)
			free(chrrom);

		NES_error = NES_ROM_ALLOC_FAILURE;
		fclose(romfile);
		return;
	}

	fread(&flags1, 1, 1, romfile);
	fread(&flags2, 1, 1, romfile);

	info.mapper_nb = (flags1 >> 4) | (flags2 & 0xF);
	info.mirroring = (flags1 >> 3) & 1 ? NES_MIRRORING_4 : flags1 & 1 ? NES_MIRRORING_V : NES_MIRRORING_H; //TODO : mirroring 4 screens => additional R.A.M.
	info.is_sram_battery_backed = (flags1 >> 1) & 1 ? true : false;
	info.is_trainer = (flags1 >> 2) & 1 ? true : false;
	info.is_vs_unisys = flags2 & 1 ? true : false;
	info.is_playchoice = (flags2 >> 1) & 1 ? true : false;

	//TODO : read the size of PRG-RAM
	fseek(romfile, 8, SEEK_CUR);

	if(info.is_trainer)
		fseek(romfile, 512, SEEK_CUR);

	fread(prgrom, 1, info.prgrom_nb * 16 * 1024, romfile);
	fread(chrrom, 1, info.chrrom_nb * 8 * 1024, romfile);

	NES_error = NES_NO_ERROR;
	rom_loaded = true;
	fclose(romfile);
}

NES_ROM::~NES_ROM()
{
	unload();
}

void NES_ROM::unload()
{
	if(!rom_loaded)
		return;

	if(prgrom)
	{
		free(prgrom);
		prgrom = NULL;
	}
	if(chrrom)
	{
		free(chrrom);
		chrrom = NULL;
	}

	info.prgrom_nb = info.chrrom_nb = info.mapper_nb = info.mirroring = 0;
	rom_loaded = info.is_sram_battery_backed = info.is_trainer = info.is_vs_unisys = info.is_playchoice = info.is_chrram = false;
}

bool NES_ROM::is_loaded()
{
	return rom_loaded;
}

unsigned int NES_ROM_Info::get_prgrom_nb() const
{
	return prgrom_nb;
}

unsigned int NES_ROM_Info::get_chrrom_nb() const
{
	return chrrom_nb;
}

unsigned int NES_ROM_Info::get_mapper_nb() const
{
	return mapper_nb;
}

string NES_ROM_Info::get_mapper_string() const
{
	switch(mapper_nb)
	{
		case NES_MAPPER_NROM:
			return "NROM";
		case NES_MAPPER_MMC1:
			return "MMC1 (SxROM boards)";
		case NES_MAPPER_UxROM:
			return "UxROM (and compatibles)";
		case NES_MAPPER_CNROM:
			return "CNROM (and compatibles)";
		case NES_MAPPER_MMC3:
			return "MMC3/MMC6";
		default:
			return "Unknown Mapper";
	}
}

int NES_ROM_Info::get_mirroring() const
{
	return mirroring;
}

string NES_ROM_Info::get_mirroring_string() const
{
	switch(mirroring)
	{
		case NES_MIRRORING_H:
			return "Horizontal";
		case NES_MIRRORING_V:
			return "Vertical";
		case NES_MIRRORING_4:
			return "4 screens";
		default:
			return "?";
	}
}

bool NES_ROM_Info::get_is_sram_battery_backed() const
{
	return is_sram_battery_backed;
}

bool NES_ROM_Info::get_is_trainer() const
{
	return is_trainer;
}

bool NES_ROM_Info::get_is_vs_unisys() const
{
	return is_vs_unisys;
}

bool NES_ROM_Info::get_is_playchoice() const
{
	return is_playchoice;
}

const uint8_t* NES_ROM::get_prgrom()
{
	return prgrom;
}

const uint8_t* NES_ROM::get_chrrom()
{
	return chrrom;
}

NES_ROM_Info NES_ROM::get_rom_info()
{
	return info;
}
