#include <SDL/SDL.h>

#include <NES.hpp>
#include <2A03.hpp>
#include <ROM.hpp>
#include <Events.hpp>

extern int NES_error;

NES::NES() : mapper(NULL), is_on(false), mapper_loaded(false)
{
	//TODO : check alloc result
	cpu = new P2A03();
	ppu = new NES_PPU();

	cpu->set_ppu(ppu);
	ppu->set_cpu(cpu);
}

NES::~NES()
{
	if(mapper_loaded)
		unload();

	cpu->unset_ppu();
	ppu->unset_cpu();

	delete cpu;
	delete ppu;
}

void NES::load(const char *path)
{
	if(mapper_loaded)
		unload();

	NES_ROM rom;
	rom.load(path);
	if(NES_get_error() != NES_NO_ERROR)
	{
		return;
	}

	mapper = mapper_from_rom(rom, ppu);
	if(!mapper)
	{
		rom.unload();
		return;
	}

	cpu->set_mapper(mapper);
	ppu->set_mapper(mapper);

	rom_info = new NES_ROM_Info;
	*rom_info = rom.get_rom_info();

	int mirroring = rom_info->get_mirroring();
	if(mirroring == NES_MIRRORING_H)
		ppu->switch_mirroring(NES_MIRRORING_H);
	else if(mirroring == NES_MIRRORING_V)
		ppu->switch_mirroring(NES_MIRRORING_V);

	make_powerup_state();

	mapper_loaded = true;
	NES_error = NES_NO_ERROR;
}

//TODO : stop the execution
void NES::unload()
{
	if(mapper)
		delete mapper;

	cpu->unset_mapper();
	ppu->unset_mapper();

	delete rom_info;
	rom_info = NULL;

	mapper_loaded = is_on = false;
}

const NES_ROM_Info* NES::get_rom_info()
{
	return rom_info;
}

void NES::next_frame()
{
		do {
			next_scanline();
		} while(ppu->get_cur_scanline() != 0);
}

void NES::next_scanline()
{
	while(cpu->next_instruction());
	ppu->render_scanline();
}

void NES::make_powerup_state()
{
	cpu->make_powerup_state();
	ppu->make_powerup_state();
}

void NES::set_screen(SDL_Surface *screen)
{
	ppu->set_screen(screen);
}

void NES::load_ini(Ini &ini)
{
	cpu->load_ini(ini);
	ppu->load_ini(ini);
}
