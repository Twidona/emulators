#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <2A03.hpp>
#include <Opcodes.hpp>
#include <Defines.hpp>
#include <Events.hpp>
#include <Ini.hpp>

using namespace std;

extern int NES_error;
extern opc_2a03 opc_2a03_table[0x100];
extern opc_2a03_am opc_2a03_modes[0x100];
extern unsigned int opc_2a03_cycles[0x100];
extern Events events;

P2A03::P2A03() : mapper (NULL), ppu (NULL)
{

}

P2A03::~P2A03()
{

}

void P2A03::load_ini(Ini &ini)
{
	controller1.load_ini(ini, "Ctrl1");
	controller2.load_ini(ini, "Ctrl2");
}

void P2A03::set_mapper(NES_Mapper *_mapper)
{
	mapper = _mapper;
}

void P2A03::unset_mapper()
{
	mapper = NULL;
}

void P2A03::set_ppu(NES_PPU *_ppu)
{
	ppu = _ppu;
}

void P2A03::unset_ppu()
{
	ppu = NULL;
}

uint8_t P2A03::read_m(NES_Memory_Space space, uint16_t addr)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0x4018) //The WRAM/SRAM is not implemented at the moment, but when it'll be, it'll be to the mapper to read it
		{
			return mapper->read_m(space, addr);
		}
		else if(addr <= 0x1FFF)
		{
			return ram[addr % 0x800];
		}
		else if(addr <= 0x3FFF)
		{
			return ppu->read_m(space, addr);
		}
		//http://wiki.nesdev.com/w/index.php/2A03
		//"the entire space from the end of CPU registers to the top of address space ($4018 through $FFFF) is available to the Game Pak."
		else if(addr <= 0x4017)
		{
			if(addr == 0x4016)
			{
				return controller1.read_next();
			}
			else if(addr == 0x4017)
			{
				return controller2.read_next();
			}
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		uint16_t addr2 = addr % 0x4000; //mirroring

		if(addr2 <= 0x1FFF)
		{
			return mapper->read_m(space, addr2);
		}
		else
		{
			return ppu->read_m(space, addr2);
		}
	}
}

void P2A03::write_m(NES_Memory_Space space, uint16_t addr, uint8_t val)
{
	if(space == NES_MEMORY_PRG)
	{
		if(addr >= 0x4018)
			mapper->write_m(space, addr, val);
		else if(addr <= 0x1FFF)
			ram[addr % 0x800] = val;
		else if(addr <= 0x3FFF)
		{
			ppu->write_m(space, addr, val);
		}
		else if(addr <= 0x4017)
		{
			if(addr == 0x4014)
			{
				ppu->write_m(space, addr, val);
			}
			else if(addr == 0x4016)
			{
				controller1.write_polling(val & 1 ? true : false);
				controller2.write_polling(val & 1 ? true : false);
			}
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		uint16_t addr2 = addr % 0x4000;

		if(addr2 <= 0x1FFF)
		{
			mapper->write_m(space, addr2, val);
		}
		else
		{
			ppu->write_m(space, addr2, val);
		}
	}
}

void P2A03::make_powerup_state()
{
	memset(ram, 0xFF, 2 * 1024);
	ram[0x8] = 0xF7;
	ram[0x9] = 0xEF;
	ram[0xA] = 0xDF;
	ram[0xF] = 0xBF;

	S = 0xFD;
	A = X = Y = 0x00;
	P.c = 0;
	P.z = 0;
	P.i = 1;
	P.d = 0;
	P.b = 0;
	P.unused = 1;
	P.v = 0;
	P.n = 0;

	cur_cycle = 0;

	PC = mapper->read_m(NES_MEMORY_PRG, 0xFFFC) | (mapper->read_m(NES_MEMORY_PRG, 0xFFFD) << 8);
}

void P2A03::make_reset_state()
{
	S -= 3;
	P.i = 1;

	PC = mapper->read_m(NES_MEMORY_PRG, 0xFFFC) | (mapper->read_m(NES_MEMORY_PRG, 0xFFFD) << 8);
}

void P2A03::start_interrupt(NES_Interrupt_Type interrupt)
{
	ram[0x100 + S] = PC >> 8;
	S--;
	ram[0x100 + S] = PC & 0xFF;
	S--;	
	ram[0x100 + S] = get_P() & 0xEF; //See Opcodes.cpp for the b flag
	S--;

	if(interrupt == NES_INTERRUPT_NMI)
		PC = mapper->read_m(NES_MEMORY_PRG, 0xFFFA) | (mapper->read_m(NES_MEMORY_PRG, 0xFFFB) << 8);
}


//returns true til the current scanline is finished
bool P2A03::next_instruction()
{
	bool ret = true;

	uint8_t opcode = read_m(NES_MEMORY_PRG, PC);
	#ifdef DEBUG
	printf("%.4X  %.2X ", PC, opcode);
	printf("A:%.2X X:%.2X Y:%.2X P:%.2X SP:%.2X CYC:%3u SL:%-3u ", A, X, Y, get_P(), S, cur_cycle * 3, ppu->get_cur_scanline());
	#endif

	PC++;

	(opc_2a03_modes[opcode])(this);
	cur_cycle += (opc_2a03_table[opcode])(this) + opc_2a03_cycles[opcode];

	if(cur_cycle > 113)
	{
		ret = false;
		cur_cycle %= 113;
	}

	printf("\n");

	return ret;
}
