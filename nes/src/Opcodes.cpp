#include <2A03.hpp>
#include <Opcodes.hpp>

#include <iostream>
#include <iomanip> //std::setfill, std::setw

using namespace std;

//TODO : valeurs de P lors de l'empilement/dépilage http://forum.6502.org/viewtopic.php?f=2&t=24&start=15
//(suite) Plus particulièrement pour la NES http://wiki.nesdev.com/w/index.php/CPU_status_flag_behavior
//(suite) http://nesdev.com/the%20%27B%27%20flag%20&%20BRK%20instruction.txt
//(suite) http://forums.nesdev.com/viewtopic.php?p=64224#p64224

opc_2a03 opc_2a03_table[0x100] = {
	opc_2a03_BRK, opc_2a03_ORA, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_ORA, opc_2a03_ASL, opc_2a03_NOP, //0x00 to 0x07
	opc_2a03_PHP, opc_2a03_ORA, opc_2a03_ASL, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_ORA, opc_2a03_ASL, opc_2a03_NOP, //0x08 to 0x0F
	opc_2a03_BPL, opc_2a03_ORA, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_ORA, opc_2a03_ASL, opc_2a03_NOP, //0x10 to 0x17
	opc_2a03_CLC, opc_2a03_ORA, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_ORA, opc_2a03_ASL, opc_2a03_NOP, //0x18 to 0x1F
	opc_2a03_JSR, opc_2a03_AND, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_BIT, opc_2a03_AND, opc_2a03_ROL, opc_2a03_NOP, //0x20 to 0x27
	opc_2a03_PLP, opc_2a03_AND, opc_2a03_ROL, opc_2a03_NOP, opc_2a03_BIT, opc_2a03_AND, opc_2a03_ROL, opc_2a03_NOP, //0x28 to 0x2F
	opc_2a03_BMI, opc_2a03_AND, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_AND, opc_2a03_ROL, opc_2a03_NOP, //0x30 to 0x37
	opc_2a03_SEC, opc_2a03_AND, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_AND, opc_2a03_ROL, opc_2a03_NOP, //0x38 to 0x3F
	opc_2a03_RTI, opc_2a03_EOR, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_EOR, opc_2a03_LSR, opc_2a03_NOP, //0x40 to 0x47
	opc_2a03_PHA, opc_2a03_EOR, opc_2a03_LSR, opc_2a03_NOP, opc_2a03_JMP, opc_2a03_EOR, opc_2a03_LSR, opc_2a03_NOP, //0x48 to 0x4F
	opc_2a03_BVC, opc_2a03_EOR, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_EOR, opc_2a03_LSR, opc_2a03_NOP, //0x50 to 0x57
	opc_2a03_CLI, opc_2a03_EOR, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_EOR, opc_2a03_LSR, opc_2a03_NOP, //0x58 to 0x5F
	opc_2a03_RTS, opc_2a03_ADC, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_ADC, opc_2a03_ROR, opc_2a03_NOP, //0x60 to 0x67
	opc_2a03_PLA, opc_2a03_ADC, opc_2a03_ROR, opc_2a03_NOP, opc_2a03_JMP, opc_2a03_ADC, opc_2a03_ROR, opc_2a03_NOP, //0x68 to 0x6F
	opc_2a03_BVS, opc_2a03_ADC, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_ADC, opc_2a03_ROR, opc_2a03_NOP, //0x70 to 0x77
	opc_2a03_SEI, opc_2a03_ADC, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_ADC, opc_2a03_ROR, opc_2a03_NOP, //0x78 to 0x7F
	opc_2a03_NOP, opc_2a03_STA, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_STY, opc_2a03_STA, opc_2a03_STX, opc_2a03_NOP, //0x80 to 0x87
	opc_2a03_DEY, opc_2a03_NOP, opc_2a03_TXA, opc_2a03_NOP, opc_2a03_STY, opc_2a03_STA, opc_2a03_STX, opc_2a03_NOP, //0x88 to 0x8F
	opc_2a03_BCC, opc_2a03_STA, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_STY, opc_2a03_STA, opc_2a03_STX, opc_2a03_NOP, //0x90 to 0x97
	opc_2a03_TYA, opc_2a03_STA, opc_2a03_TXS, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_STA, opc_2a03_NOP, opc_2a03_NOP, //0x98 to 0x9F
	opc_2a03_LDY, opc_2a03_LDA, opc_2a03_LDX, opc_2a03_NOP, opc_2a03_LDY, opc_2a03_LDA, opc_2a03_LDX, opc_2a03_NOP, //0xA0 to 0xA7
	opc_2a03_TAY, opc_2a03_LDA, opc_2a03_TAX, opc_2a03_NOP, opc_2a03_LDY, opc_2a03_LDA, opc_2a03_LDX, opc_2a03_NOP, //0xA8 to 0xAF
	opc_2a03_BCS, opc_2a03_LDA, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_LDY, opc_2a03_LDA, opc_2a03_LDX, opc_2a03_NOP, //0xB0 to 0xB7
	opc_2a03_CLV, opc_2a03_LDA, opc_2a03_TSX, opc_2a03_NOP, opc_2a03_LDY, opc_2a03_LDA, opc_2a03_LDX, opc_2a03_NOP, //0xB8 to 0xBF
	opc_2a03_CPY, opc_2a03_CMP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_CPY, opc_2a03_CMP, opc_2a03_DEC, opc_2a03_NOP, //0xC0 to 0xC7
	opc_2a03_INY, opc_2a03_CMP, opc_2a03_DEX, opc_2a03_NOP, opc_2a03_CPY, opc_2a03_CMP, opc_2a03_DEC, opc_2a03_NOP, //0xC8 to 0xCF
	opc_2a03_BNE, opc_2a03_CMP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_CMP, opc_2a03_DEC, opc_2a03_NOP, //0xD0 to 0xD7
	opc_2a03_CLD, opc_2a03_CMP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_CMP, opc_2a03_DEC, opc_2a03_NOP, //0xD8 to 0xDF
	opc_2a03_CPX, opc_2a03_SBC, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_CPX, opc_2a03_SBC, opc_2a03_INC, opc_2a03_NOP, //0xE0 to 0xE7
	opc_2a03_INX, opc_2a03_SBC, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_CPX, opc_2a03_SBC, opc_2a03_INC, opc_2a03_NOP, //0xE8 to 0xEF
	opc_2a03_BEQ, opc_2a03_SBC, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_SBC, opc_2a03_INC, opc_2a03_NOP, //0xF0 to 0xF7
	opc_2a03_SED, opc_2a03_SBC, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_NOP, opc_2a03_SBC, opc_2a03_INC, opc_2a03_NOP //0xF8 to 0xFF
};

unsigned int opc_2a03_INC(P2A03 *cpu)
{
	cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	uint8_t val = cpu->oper + 1;

	cpu->P.z = val == 0 ? 1 : 0;
	cpu->P.n = val >> 7;

	cpu->write_m(NES_MEMORY_PRG, cpu->addr, cpu->oper);
	cpu->write_m(NES_MEMORY_PRG, cpu->addr, val);

	#ifdef DEBUG
	printf("INC $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_DEC(P2A03 *cpu)
{
	cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	uint8_t val = cpu->oper - 1;
	cpu->write_m(NES_MEMORY_PRG, cpu->addr, val);

	cpu->P.z = val == 0 ? 1 : 0;
	cpu->P.n = val >> 7;

	#ifdef DEBUG
	printf("DEC $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_LDA(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->A = cpu->oper;

	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;

	#ifdef DEBUG
	printf("LDA ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABX || cpu->am == NES_AM_ABY || cpu->am == NES_AM_INY)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_LDX(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->X = cpu->oper;

	cpu->P.z = cpu->X == 0 ? 1 : 0;
	cpu->P.n = cpu->X >> 7;

	#ifdef DEBUG
	printf("LDX ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABY)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_LDY(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->Y = cpu->oper;

	cpu->P.z = cpu->Y == 0 ? 1 : 0;
	cpu->P.n = cpu->Y >> 7;

	#ifdef DEBUG
	printf("LDY ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABX)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_STA(P2A03 *cpu)
{
	cpu->write_m(NES_MEMORY_PRG, cpu->addr, cpu->A);

	#ifdef DEBUG
	printf("STA $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_STX(P2A03 *cpu)
{
	cpu->write_m(NES_MEMORY_PRG, cpu->addr, cpu->X);

	#ifdef DEBUG
	printf("STX $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_STY(P2A03 *cpu)
{
	cpu->write_m(NES_MEMORY_PRG, cpu->addr, cpu->Y);

	#ifdef DEBUG
	printf("STY $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_CMP(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->P.z = cpu->A == cpu->oper ? 1 : 0;
	cpu->P.c = cpu->A >= cpu->oper ? 1 : 0;
	cpu->P.n = (cpu->A - cpu->oper) >> 7;

	#ifdef DEBUG
	printf("CMP ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABX || cpu->am == NES_AM_ABY || cpu->am == NES_AM_INY)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_CPX(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->P.z = cpu->X == cpu->oper ? 1 : 0;
	cpu->P.c = cpu->X >= cpu->oper ? 1 : 0;
	cpu->P.n = (cpu->X - cpu->oper) >> 7;

	#ifdef DEBUG
	printf("CPX ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_CPY(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->P.z = cpu->Y == cpu->oper ? 1 : 0;
	cpu->P.c = cpu->Y >= cpu->oper ? 1 : 0;
	cpu->P.n = (cpu->Y - cpu->oper) >> 7;

	#ifdef DEBUG
	printf("CPY ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_SBC(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	uint16_t result = (uint16_t) cpu->A + (uint16_t) ((uint8_t) cpu->oper ^ 0xFF) + cpu->P.c;
	int16_t tmp = (int8_t) cpu->A + (int8_t) (cpu->oper ^ 0xFF) + cpu->P.c;
	cpu->A = result & 0xFF;
	cpu->P.c = result >> 8;
	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;
	cpu->P.v = (tmp < -128 || tmp > 127) ? 1 : 0;

	#ifdef DEBUG
	printf("SBC ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABX || cpu->am == NES_AM_ABY || cpu->am == NES_AM_INY)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_ADC(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	uint16_t result = (uint16_t) cpu->A + (uint16_t) cpu->oper + (uint16_t) cpu->P.c;
	int16_t tmp = (int8_t) cpu->A + (int8_t) cpu->oper + cpu->P.c;
	cpu->A = result & 0xFF;
	cpu->P.c = result >> 8;
	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;


	cpu->P.v = (tmp < -128 || tmp > 127) ? 1 : 0;

	#ifdef DEBUG
	printf("ADC ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABX || cpu->am == NES_AM_ABY || cpu->am == NES_AM_INY)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_LSR(P2A03 *cpu)
{
	if(cpu->am != NES_AM_ACC)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);
	else
		cpu->oper = cpu->A;

	cpu->P.c = cpu->oper & 1;
	cpu->oper >>= 1;

	if(cpu->am == NES_AM_ACC)
		cpu->A = cpu->oper;

	cpu->P.z = cpu->oper == 0 ? 1 : 0;
	cpu->P.n = cpu->oper >> 7;

	if(cpu->am != NES_AM_ACC)
		cpu->write_m(NES_MEMORY_PRG, cpu->addr, cpu->oper);

	#ifdef DEBUG
	printf("LSR $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_ASL(P2A03 *cpu)
{
	if(cpu->am != NES_AM_ACC)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);
	else
		cpu->oper = cpu->A;

	cpu->P.c = cpu->oper >> 7;
	cpu->oper <<= 1;

	if(cpu->am == NES_AM_ACC)
		cpu->A = cpu->oper;

	cpu->P.z = cpu->oper == 0 ? 1 : 0;
	cpu->P.n = cpu->oper >> 7;

	if(cpu->am != NES_AM_ACC)
		cpu->write_m(NES_MEMORY_PRG, cpu->addr, cpu->oper);

	#ifdef DEBUG
	printf("ASL $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_ROR(P2A03 *cpu)
{
	uint8_t old_carry = cpu->P.c;

	if(cpu->am != NES_AM_ACC)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);
	else
		cpu->oper = cpu->A;

	cpu->P.c = cpu->oper & 0x01;
	cpu->oper = (cpu->oper >> 1) | (old_carry << 7);

	if(cpu->am == NES_AM_ACC)
		cpu->A = cpu->oper;

	cpu->P.z = cpu->oper == 0 ? 1 : 0;
	cpu->P.n = cpu->oper >> 7;

	if(cpu->am != NES_AM_ACC)
		cpu->write_m(NES_MEMORY_PRG, cpu->addr, cpu->oper);

	#ifdef DEBUG
	printf("ROR $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_ROL(P2A03 *cpu)
{
	uint8_t old_c = cpu->P.c;

	if(cpu->am != NES_AM_ACC)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);
	else
		cpu->oper = cpu->A;

	cpu->P.c = cpu->oper >> 7;

	cpu->oper <<= 1;
	cpu->oper |= old_c;

	if(cpu->am == NES_AM_ACC)
		cpu->A = cpu->oper;

	if(cpu->am != NES_AM_ACC)
		cpu->write_m(NES_MEMORY_PRG, cpu->addr, cpu->oper);

	cpu->P.z = cpu->oper == 0 ? 1 : 0;
	cpu->P.n = cpu->oper >> 7;

	#ifdef DEBUG
	printf("ROL $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_EOR(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->A ^= cpu->oper;
	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;

	#ifdef DEBUG
	printf("EOR ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABX || cpu->am == NES_AM_ABY || cpu->am == NES_AM_INY)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_ORA(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->A = cpu->A | cpu->oper;
	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;

	#ifdef DEBUG
	printf("ORA ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABX || cpu->am == NES_AM_ABY || cpu->am == NES_AM_INY)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_AND(P2A03 *cpu)
{
	if(cpu->am != NES_AM_IMM)
		cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	cpu->A = cpu->A & cpu->oper;

	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;

	#ifdef DEBUG
	printf("AND ");
	if(cpu->am == NES_AM_IMM) printf("#$%02X", cpu->oper);
	else printf("$%04X", cpu->addr);
	#endif

	if(cpu->am == NES_AM_ABX || cpu->am == NES_AM_ABY || cpu->am == NES_AM_INY)
		return cpu->page_crossed_cycle;

	return 0;
}

unsigned int opc_2a03_BIT(P2A03 *cpu)
{
	cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->addr);

	uint8_t result = cpu->A & cpu->oper;

	cpu->P.z = result == 0 ? 1 : 0;
	cpu->P.v = cpu->oper >> 6;
	cpu->P.n = cpu->oper >> 7;

	#ifdef DEBUG
	printf("BIT $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_JMP(P2A03 *cpu)
{
	cpu->PC = cpu->addr;

	#ifdef DEBUG
	printf("JMP $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_PHP(P2A03 *cpu)
{
	cpu->ram[0x100 + cpu->S] = get_P_2() | (1 << 4); //PHP always push P with b set to 1
	//http://forum.6502.org/viewtopic.php?t=1659
	cpu->S--;

	#ifdef DEBUG
	printf("PHP");
	#endif

	return 0;
}

unsigned int opc_2a03_PLP(P2A03 *cpu)
{
	cpu->S++;
	set_P(cpu->ram[0x100 + cpu->S]);

	//http://forum.6502.org/viewtopic.php?f=2&t=24&start=15
	//Plus particulièrement pour la NES http://wiki.nesdev.com/w/index.php/CPU_status_flag_behavior
	//http://nesdev.com/the%20%27B%27%20flag%20&%20BRK%20instruction.txt
	//http://forums.nesdev.com/viewtopic.php?p=64224#p64224
	cpu->P.b = 0;
	cpu->P.unused = 1;

	#ifdef DEBUG
	printf("PLP");
	#endif

	return 0;
}

unsigned int opc_2a03_PHA(P2A03 *cpu)
{
	cpu->ram[0x100 + cpu->S] = cpu->A;
	cpu->S--;

	#ifdef DEBUG
	printf("PHA");
	#endif

	return 0;
}

unsigned int opc_2a03_PLA(P2A03 *cpu)
{
	cpu->S++;
	cpu->A = cpu->ram[0x100 + cpu->S];

	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;

	#ifdef DEBUG
	printf("PLA");
	#endif

	return 0;
}


unsigned int opc_2a03_BEQ(P2A03 *cpu)
{
	int8_t op = cpu->oper;

	#ifdef DEBUG
	printf("BEQ $%04X", cpu->PC + op);
	#endif

	if(cpu->P.z == 1)
	{
		cpu->PC += op;

		if(cpu->PC >> 8 != (cpu->PC - op) >> 8)
			return 2;

		return 1;
	}

	return 0;
}

unsigned int opc_2a03_BNE(P2A03 *cpu)
{
	int8_t op = cpu->oper;

	#ifdef DEBUG
	printf("BNE $%04X", cpu->PC + op);
	#endif

	if(cpu->P.z == 0)
	{
		cpu->PC += op;

		if(cpu->PC >> 8 != (cpu->PC - op) >> 8)
			return 2;

		return 1;
	}

	return 0;
}

unsigned int opc_2a03_BPL(P2A03 *cpu)
{
	int8_t op = cpu->oper;

	#ifdef DEBUG
	printf("BPL $%04X", cpu->PC + op);
	#endif

	if(cpu->P.n == 0)
	{
		cpu->PC += op;

		if(cpu->PC >> 8 != (cpu->PC - op) >> 8)
			return 2;

		return 1;
	}

	return 0;
}

unsigned int opc_2a03_BMI(P2A03 *cpu)
{
	int8_t op = cpu->oper;

	#ifdef DEBUG
	printf("BMI $%04X", cpu->PC + op);
	#endif

	if(cpu->P.n == 1)
	{
		cpu->PC += op;

		if(cpu->PC >> 8 != (cpu->PC - op) >> 8)
			return 2;

		return 1;
	}

	return 0;
}

unsigned int opc_2a03_BVC(P2A03 *cpu)
{
	int8_t op = cpu->oper;

	#ifdef DEBUG
	printf("BVC $%04X", cpu->PC + op);
	#endif

	if(cpu->P.v == 0)
	{
		cpu->PC += op;

		if(cpu->PC >> 8 != (cpu->PC - op) >> 8)
			return 2;

		return 1;
	}

	return 0;
}

unsigned int opc_2a03_BVS(P2A03 *cpu)
{
	int8_t op = cpu->oper;

	#ifdef DEBUG
	printf("BVS $%04X", cpu->PC + op);
	#endif

	if(cpu->P.v == 1)
	{
		cpu->PC += op;

		if(cpu->PC >> 8 != (cpu->PC - op) >> 8)
			return 2;

		return 1;
	}

	return 0;
}

unsigned int opc_2a03_BCC(P2A03 *cpu)
{
	int8_t op = cpu->oper;

	#ifdef DEBUG
	printf("BCC $%04X", cpu->PC + op);
	#endif

	if(cpu->P.c == 0)
	{
		cpu->PC += op;

		if(cpu->PC >> 8 != (cpu->PC - op) >> 8)
			return 2;

		return 1;
	}

	return 0;
}

unsigned int opc_2a03_BCS(P2A03 *cpu)
{
	int8_t op = cpu->oper;

	#ifdef DEBUG
	printf("BCS $%04X", cpu->PC + op);
	#endif

	if(cpu->P.c == 1)
	{
		cpu->PC += op;

		if(cpu->PC >> 8 != (cpu->PC - op) >> 8)
			return 2;

		return 1;
	}

	return 0;
}

unsigned int opc_2a03_CLI(P2A03 *cpu)
{
	cpu->P.i = 0; 

	#ifdef DEBUG
	printf("CLI");
	#endif

	return 0;
}

unsigned int opc_2a03_SEI(P2A03 *cpu)
{
	cpu->P.i = 1;

	#ifdef DEBUG
	printf("SEI");
	#endif

	return 0;
}

unsigned int opc_2a03_CLV(P2A03 *cpu)
{
	cpu->P.v = 0; 

	#ifdef DEBUG
	printf("CLV");
	#endif

	return 0;
}

unsigned int opc_2a03_CLC(P2A03 *cpu)
{
	cpu->P.c = 0; 

	#ifdef DEBUG
	printf("CLC");
	#endif

	return 0;
}

unsigned int opc_2a03_SEC(P2A03 *cpu)
{
	cpu->P.c = 1;

	#ifdef DEBUG
	printf("SEC");
	#endif

	return 0;
}

unsigned int opc_2a03_CLD(P2A03 *cpu)
{
	cpu->P.d = 0; 

	#ifdef DEBUG
	printf("CLD");
	#endif

	return 0;
}

unsigned int opc_2a03_SED(P2A03 *cpu)
{
	cpu->P.d = 1;

	#ifdef DEBUG
	printf("SED");
	#endif

	return 0;
}


unsigned int opc_2a03_TXA(P2A03 *cpu)
{
	cpu->A = cpu->X;

	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;

	#ifdef DEBUG
	printf("TXA");
	#endif

	return 0;
}

unsigned int opc_2a03_TYA(P2A03 *cpu)
{
	cpu->A = cpu->Y;

	cpu->P.z = cpu->A == 0 ? 1 : 0;
	cpu->P.n = cpu->A >> 7;

	#ifdef DEBUG
	printf("TYA");
	#endif

	return 0;
}

unsigned int opc_2a03_TXS(P2A03 *cpu)
{
	cpu->S = cpu->X;

	#ifdef DEBUG
	printf("TXS");
	#endif

	return 0;
}

unsigned int opc_2a03_TSX(P2A03 *cpu)
{
	cpu->X = cpu->S;

	cpu->P.z = cpu->X == 0 ? 1 : 0;
	cpu->P.n = cpu->X >> 7;

	#ifdef DEBUG
	printf("TSX");
	#endif

	return 0;
}

unsigned int opc_2a03_TAY(P2A03 *cpu)
{
	cpu->Y = cpu->A;

	cpu->P.z = cpu->Y == 0 ? 1 : 0;
	cpu->P.n = cpu->Y >> 7;

	#ifdef DEBUG
	printf("TAY");
	#endif

	return 0;
}

unsigned int opc_2a03_TAX(P2A03 *cpu)
{
	cpu->X = cpu->A;

	cpu->P.z = cpu->X == 0 ? 1 : 0;
	cpu->P.n = cpu->X >> 7;

	#ifdef DEBUG
	printf("TAX");
	#endif

	return 0;
}

unsigned int opc_2a03_BRK(P2A03 *cpu)
{
	cpu->ram[0x100 + cpu->S] = (cpu->PC + 1) >> 8;
	cpu->S--;
	cpu->ram[0x100 + cpu->S] = (cpu->PC + 1) & 0xFF;
	cpu->S--;	
	cpu->ram[0x100 + cpu->S] = get_P_2() | (3 << 4);
	cpu->S--;

	cpu->PC = (cpu->read_m(NES_MEMORY_PRG, 0xFFFE) | (cpu->read_m(NES_MEMORY_PRG, 0xFFFF) << 8));

	#ifdef DEBUG
	printf("BRK", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_JSR(P2A03 *cpu)
{
	cpu->PC -= 1; // -= 1 because the adress pushed is the return adress *minus 1*

	cpu->ram[0x100 + cpu->S] = cpu->PC >> 8;
	cpu->S--;
	cpu->ram[0x100 + cpu->S] = cpu->PC & 0xFF;
	cpu->S--;	

	cpu->PC = cpu->addr;

	#ifdef DEBUG
	printf("JSR $%04X", cpu->addr);
	#endif

	return 0;
}

unsigned int opc_2a03_RTI(P2A03 *cpu)
{
	cpu->S++;
	uint8_t p_reg = cpu->ram[0x100 + cpu->S];
	set_P(p_reg);
	cpu->P.b = 0;
	cpu->P.unused = 1;

	cpu->S++;
	uint8_t val1 = cpu->ram[0x100 + cpu->S];
	cpu->S++;
	uint8_t val2 = cpu->ram[0x100 + cpu->S];

	cpu->PC = (val1 | (val2 << 8));

	#ifdef DEBUG
	printf("RTI");
	#endif

	return 0;
}

unsigned int opc_2a03_RTS(P2A03 *cpu)
{
	cpu->S++;
	uint8_t val1 = cpu->ram[0x100 + cpu->S];
	cpu->S++;
	uint8_t val2 = cpu->ram[0x100 + cpu->S];

	cpu->PC = (val1 | (val2 << 8)) + 1;

	#ifdef DEBUG
	printf("RTS");
	#endif

	return 0;
}

unsigned int opc_2a03_DEY(P2A03 *cpu)
{
	cpu->Y = cpu->Y - 1;

	cpu->P.z = cpu->Y == 0 ? 1 : 0;
	cpu->P.n = cpu->Y >> 7;

	#ifdef DEBUG
	printf("DEY");
	#endif

	return 0;
}

unsigned int opc_2a03_INY(P2A03 *cpu)
{
	cpu->Y += 1;

	cpu->P.z = cpu->Y == 0 ? 1 : 0;
	cpu->P.n = cpu->Y >> 7;

	#ifdef DEBUG
	printf("INY");
	#endif

	return 0;
}

unsigned int opc_2a03_DEX(P2A03 *cpu)
{
	cpu->X -= 1;

	cpu->P.z = cpu->X == 0 ? 1 : 0;
	cpu->P.n = cpu->X >> 7;

	#ifdef DEBUG
	printf("DEX");
	#endif

	return 0;
}

unsigned int opc_2a03_INX(P2A03 *cpu)
{
	cpu->X++;

	cpu->P.z = cpu->X == 0 ? 1 : 0;
	cpu->P.n = cpu->X >> 7;

	#ifdef DEBUG
	printf("INX");
	#endif

	return 0;
}

unsigned int opc_2a03_NOP(P2A03 *cpu)
{
	#ifdef DEBUG
	printf("NOP/Unknown");
	#endif

	return 0;
}

opc_2a03_am opc_2a03_modes[0x100] = {
	opc_2a03_am_IMP, opc_2a03_am_INX, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_NOP, //0x00 to 0x07
	opc_2a03_am_IMP, opc_2a03_am_IMM, opc_2a03_am_ACC, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_NOP, //0x08 to 0x0F
	opc_2a03_am_REL, opc_2a03_am_INY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZPX, opc_2a03_am_ZPX, opc_2a03_am_NOP, //0x10 to 0x17
	opc_2a03_am_IMP, opc_2a03_am_ABY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ABX, opc_2a03_am_ABX, opc_2a03_am_NOP, //0x18 to 0x1F
	opc_2a03_am_ABS, opc_2a03_am_INX, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_NOP, //0x20 to 0x27
	opc_2a03_am_IMP, opc_2a03_am_IMM, opc_2a03_am_ACC, opc_2a03_am_NOP, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_NOP, //0x28 to 0x2F
	opc_2a03_am_REL, opc_2a03_am_INY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZPX, opc_2a03_am_ZPX, opc_2a03_am_NOP, //0x30 to 0x37
	opc_2a03_am_IMP, opc_2a03_am_ABY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ABX, opc_2a03_am_ABX, opc_2a03_am_NOP, //0x38 to 0x3F
	opc_2a03_am_IMP, opc_2a03_am_INX, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_NOP, //0x40 to 0x47
	opc_2a03_am_IMP, opc_2a03_am_IMM, opc_2a03_am_ACC, opc_2a03_am_NOP, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_NOP, //0x48 to 0x4F
	opc_2a03_am_REL, opc_2a03_am_INY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZPX, opc_2a03_am_ZPX, opc_2a03_am_NOP, //0x50 to 0x57
	opc_2a03_am_IMP, opc_2a03_am_ABY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ABX, opc_2a03_am_ABX, opc_2a03_am_NOP, //0x58 to 0x5F
	opc_2a03_am_IMP, opc_2a03_am_INX, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_NOP, //0x60 to 0x67
	opc_2a03_am_IMP, opc_2a03_am_IMM, opc_2a03_am_ACC, opc_2a03_am_NOP, opc_2a03_am_IND, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_NOP, //0x68 to 0x6F
	opc_2a03_am_REL, opc_2a03_am_INY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZPX, opc_2a03_am_ZPX, opc_2a03_am_NOP, //0x70 to 0x77
	opc_2a03_am_IMP, opc_2a03_am_ABY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ABX, opc_2a03_am_ABX, opc_2a03_am_NOP, //0x78 to 0x7F
	opc_2a03_am_NOP, opc_2a03_am_INX, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_NOP, //0x80 to 0x87
	opc_2a03_am_IMP, opc_2a03_am_NOP, opc_2a03_am_IMP, opc_2a03_am_NOP, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_NOP, //0x88 to 0x8F
	opc_2a03_am_REL, opc_2a03_am_INY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZPX, opc_2a03_am_ZPX, opc_2a03_am_ZPY, opc_2a03_am_NOP, //0x90 to 0x97
	opc_2a03_am_IMP, opc_2a03_am_ABY, opc_2a03_am_IMP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ABX, opc_2a03_am_NOP, opc_2a03_am_NOP, //0x98 to 0x9F
	opc_2a03_am_IMM, opc_2a03_am_INX, opc_2a03_am_IMM, opc_2a03_am_NOP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_NOP, //0xA0 to 0xA7
	opc_2a03_am_IMP, opc_2a03_am_IMM, opc_2a03_am_IMP, opc_2a03_am_NOP, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_NOP, //0xA8 to 0xAF
	opc_2a03_am_REL, opc_2a03_am_INY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZPX, opc_2a03_am_ZPX, opc_2a03_am_ZPY, opc_2a03_am_NOP, //0xB0 to 0xB7
	opc_2a03_am_IMP, opc_2a03_am_ABY, opc_2a03_am_IMP, opc_2a03_am_NOP, opc_2a03_am_ABX, opc_2a03_am_ABX, opc_2a03_am_ABY, opc_2a03_am_NOP, //0xB8 to 0xBF
	opc_2a03_am_IMM, opc_2a03_am_INX, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_NOP, //0xC0 to 0xC7
	opc_2a03_am_IMP, opc_2a03_am_IMM, opc_2a03_am_IMP, opc_2a03_am_NOP, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_NOP, //0xC8 to 0xCF
	opc_2a03_am_REL, opc_2a03_am_INY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZPX, opc_2a03_am_ZPX, opc_2a03_am_NOP, //0xD0 to 0xD7
	opc_2a03_am_IMP, opc_2a03_am_ABY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ABX, opc_2a03_am_ABX, opc_2a03_am_NOP, //0xD8 to 0xDF
	opc_2a03_am_IMM, opc_2a03_am_INX, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_ZP, opc_2a03_am_NOP, //0xE0 to 0xE7
	opc_2a03_am_IMP, opc_2a03_am_IMM, opc_2a03_am_IMP, opc_2a03_am_NOP, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_ABS, opc_2a03_am_NOP, //0xE8 to 0xEF
	opc_2a03_am_REL, opc_2a03_am_INY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ZPX, opc_2a03_am_ZPX, opc_2a03_am_NOP, //0xF0 to 0xF7
	opc_2a03_am_IMP, opc_2a03_am_ABY, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_NOP, opc_2a03_am_ABX, opc_2a03_am_ABX, opc_2a03_am_NOP //0xF8 to 0xFF
};

void opc_2a03_am_IMP(P2A03 *cpu)
{
	cpu->am = NES_AM_IMP;
}

void opc_2a03_am_IND(P2A03 *cpu)
{
	cpu->am = NES_AM_IND;

	uint16_t source_addr = cpu->read_m(NES_MEMORY_PRG, cpu->PC) | (cpu->read_m(NES_MEMORY_PRG, cpu->PC + 1) << 8);
	cpu->PC += 2;

	cpu->addr = cpu->read_m(NES_MEMORY_PRG, source_addr) | (cpu->read_m(NES_MEMORY_PRG, source_addr & 0xFF == 0xFF ? source_addr & 0xFF00 : source_addr + 1) << 8);
}

void opc_2a03_am_IMM(P2A03 *cpu)
{
	cpu->am = NES_AM_IMM;

	cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->PC);
	(cpu->PC)++;
}

void opc_2a03_am_REL(P2A03 *cpu)
{
	cpu->am = NES_AM_REL;

	cpu->oper = cpu->read_m(NES_MEMORY_PRG, cpu->PC);
	(cpu->PC)++;
}

void opc_2a03_am_NOP(P2A03 *cpu)
{
	cpu->am = NES_AM_NOP;
}

void opc_2a03_am_ACC(P2A03 *cpu)
{
	cpu->am = NES_AM_ACC;
}

void opc_2a03_am_ABS(P2A03 *cpu)
{
	cpu->am = NES_AM_ABS;

	cpu->addr = cpu->read_m(NES_MEMORY_PRG, cpu->PC) | (cpu->read_m(NES_MEMORY_PRG, cpu->PC + 1) << 8);
	cpu->PC += 2;
}

void opc_2a03_am_ABX(P2A03 *cpu)
{
	cpu->am = NES_AM_ABX;

	cpu->addr = (cpu->read_m(NES_MEMORY_PRG, cpu->PC) | (cpu->read_m(NES_MEMORY_PRG, cpu->PC + 1) << 8)) + cpu->X;
	cpu->PC += 2;

	cpu->page_crossed_cycle = (cpu->addr - cpu->X) >> 8 != cpu->addr >> 8 ? 1 : 0;
}

void opc_2a03_am_ABY(P2A03 *cpu)
{
	cpu->am = NES_AM_ABY;

	cpu->addr = (cpu->read_m(NES_MEMORY_PRG, cpu->PC) | (cpu->read_m(NES_MEMORY_PRG, cpu->PC + 1) << 8)) + cpu->Y;
	cpu->PC += 2;

	cpu->page_crossed_cycle = (cpu->addr - cpu->Y) >> 8 != cpu->addr >> 8 ? 1 : 0;
}

void opc_2a03_am_ZP(P2A03 *cpu)
{
	cpu->am = NES_AM_ZP;

	cpu->addr = cpu->read_m(NES_MEMORY_PRG, cpu->PC);
	cpu->PC += 1;
}

void opc_2a03_am_ZPX(P2A03 *cpu)
{
	cpu->am = NES_AM_ZPX;

	uint8_t addr = cpu->read_m(NES_MEMORY_PRG, cpu->PC);
	addr += cpu->X;
	cpu->PC += 1;

	cpu->addr = addr;
}

void opc_2a03_am_ZPY(P2A03 *cpu)
{
	cpu->am = NES_AM_ZPY;

	uint8_t addr = cpu->read_m(NES_MEMORY_PRG, cpu->PC);
	addr += cpu->Y;
	cpu->PC += 1;

	cpu->addr = addr;
}

void opc_2a03_am_INX(P2A03 *cpu)
{
	cpu->am = NES_AM_INX;

	uint8_t zp_addr = cpu->read_m(NES_MEMORY_PRG, cpu->PC);
	zp_addr += cpu->X;
	uint8_t zp_addr_1 = zp_addr + 1;
	(cpu->PC)++;

	cpu->addr = cpu->read_m(NES_MEMORY_PRG, zp_addr) | (cpu->read_m(NES_MEMORY_PRG, zp_addr_1) << 8);
}

void opc_2a03_am_INY(P2A03 *cpu)
{
	cpu->am = NES_AM_INY;

	uint8_t zp_addr = cpu->read_m(NES_MEMORY_PRG, cpu->PC);
	uint8_t zp_addr_1 = zp_addr + 1;
	(cpu->PC)++;

	cpu->addr = (cpu->read_m(NES_MEMORY_PRG, zp_addr) | (cpu->read_m(NES_MEMORY_PRG, zp_addr_1) << 8)) + cpu->Y;

	cpu->page_crossed_cycle = (cpu->addr - cpu->Y) >> 8 != cpu->addr >> 8 ? 1 : 0;
}

unsigned int opc_2a03_cycles[0x100] = {
	7, 6, 0, 0, 0, 3, 5, 0, //0x00 to 0x07
	3, 2, 2, 0, 0, 4, 6, 0, //0x08 to 0x0F
	2, 5, 0, 0, 0, 4, 6, 0, //0x10 to 0x17
	2, 4, 0, 0, 0, 4, 7, 0, //0x18 to 0x1F
	6, 6, 0, 0, 3, 3, 5, 0, //0x20 to 0x27
	4, 2, 2, 0, 4, 4, 6, 0, //0x28 to 0x2F
	2, 5, 0, 0, 0, 4, 6, 0, //0x30 to 0x37
	2, 4, 0, 0, 0, 4, 7, 0, //0x38 to 0x3F
	6, 6, 0, 0, 0, 3, 5, 0, //0x40 to 0x47
	3, 2, 2, 0, 3, 4, 6, 0, //0x48 to 0x4F
	2, 5, 0, 0, 0, 4, 6, 0, //0x50 to 0x57
	2, 4, 0, 0, 0, 4, 7, 0, //0x58 to 0x5F
	6, 6, 0, 0, 0, 3, 5, 0, //0x60 to 0x67
	4, 2, 2, 0, 5, 4, 6, 0, //0x68 to 0x6F
	2, 5, 0, 0, 0, 4, 6, 0, //0x70 to 0x77
	2, 4, 0, 0, 0, 4, 7, 0, //0x78 to 0x7F
	0, 6, 0, 0, 3, 3, 3, 0, //0x80 to 0x87
	2, 0, 2, 0, 4, 4, 4, 0, //0x88 to 0x8F
	2, 6, 0, 0, 4, 4, 4, 0, //0x90 to 0x97
	2, 5, 2, 0, 0, 5, 0, 0, //0x98 to 0x9F
	2, 6, 2, 0, 3, 3, 3, 0, //0xA0 to 0xA7
	2, 2, 2, 0, 4, 4, 4, 0, //0xA8 to 0xAF
	2, 5, 0, 0, 4, 4, 4, 0, //0xB0 to 0xB7
	2, 4, 2, 0, 4, 4, 4, 0, //0xB8 to 0xBF
	2, 6, 0, 0, 3, 3, 5, 0, //0xC0 to 0xC7
	2, 2, 2, 0, 4, 4, 6, 0, //0xC8 to 0xCF
	2, 5, 0, 0, 0, 4, 6, 0, //0xD0 to 0xD7
	2, 4, 0, 0, 0, 4, 7, 0, //0xD8 to 0xDF
	2, 6, 0, 0, 3, 3, 5, 0, //0xE0 to 0xE7
	2, 2, 2, 0, 4, 4, 6, 0, //0xE8 to 0xEF
	2, 5, 0, 0, 0, 4, 6, 0, //0xF0 to 0xF7
	2, 4, 0, 0, 0, 4, 7, 0 //0xF8 to 0xFF
};
