#include <stdint.h>
#include <stdio.h>

#include <SDL/SDL.h>

#include <PPU.hpp>
#include <Mapper.hpp>
#include <Defines.hpp>

NES_PPU::NES_PPU()
{
	nt1 = nt2 = nt3 = nt4 = NULL;
	screen = NULL;

	sprite_0_hit = 0;

	real_nt1 = new uint8_t[0x400];
	real_nt2 = new uint8_t[0x400];

	nt1 = nt2 = nt3 = nt4 = real_nt1;
	mirroring = NES_MIRRORING_1SA;

	pal[0x00].r = 0x75;
	pal[0x00].g = 0x75;
	pal[0x00].b = 0x75;

	pal[0x01].r = 0x27;
	pal[0x01].g = 0x1B;
	pal[0x01].b = 0x8F;

	pal[0x02].r = 0x00;
	pal[0x02].g = 0x00;
	pal[0x02].b = 0xAB;

	pal[0x03].r = 0x47;
	pal[0x03].g = 0x00;
	pal[0x03].b = 0x9F;

	pal[0x04].r = 0x8F;
	pal[0x04].g = 0x00;
	pal[0x04].b = 0x77;

	pal[0x05].r = 0xAB;
	pal[0x05].g = 0x00;
	pal[0x05].b = 0x13;

	pal[0x06].r = 0xA7;
	pal[0x06].g = 0x00;
	pal[0x06].b = 0x00;

	pal[0x07].r = 0x7F;
	pal[0x07].g = 0x0B;
	pal[0x07].b = 0x00;

	pal[0x08].r = 0x43;
	pal[0x08].g = 0x2F;
	pal[0x08].b = 0x00;

	pal[0x09].r = 0x00;
	pal[0x09].g = 0x47;
	pal[0x09].b = 0x00;

	pal[0x0A].r = 0x00;
	pal[0x0A].g = 0x51;
	pal[0x0A].b = 0x00;

	pal[0x0B].r = 0x00;
	pal[0x0B].g = 0x3F;
	pal[0x0B].b = 0x17;

	pal[0x0C].r = 0x1B;
	pal[0x0C].g = 0x3F;
	pal[0x0C].b = 0x5F;

	pal[0x0D].r = 0x00;
	pal[0x0D].g = 0x00;
	pal[0x0D].b = 0x00;

	pal[0x0E].r = 0x00;
	pal[0x0E].g = 0x00;
	pal[0x0E].b = 0x00;

	pal[0x0F].r = 0x00;
	pal[0x0F].g = 0x00;
	pal[0x0F].b = 0x00;

	pal[0x10].r = 0xBC;
	pal[0x10].g = 0xBC;
	pal[0x10].b = 0xBC;

	pal[0x11].r = 0x00;
	pal[0x11].g = 0x73;
	pal[0x11].b = 0xEF;

	pal[0x12].r = 0x23;
	pal[0x12].g = 0x3B;
	pal[0x12].b = 0xEF;

	pal[0x13].r = 0x83;
	pal[0x13].g = 0x00;
	pal[0x13].b = 0xF3;

	pal[0x14].r = 0xBF;
	pal[0x14].g = 0x00;
	pal[0x14].b = 0xBF;

	pal[0x15].r = 0xE7;
	pal[0x15].g = 0x00;
	pal[0x15].b = 0x5B;

	pal[0x16].r = 0xDB;
	pal[0x16].g = 0x2B;
	pal[0x16].b = 0x00;

	pal[0x17].r = 0xCB;
	pal[0x17].g = 0x4F;
	pal[0x17].b = 0x0F;

	pal[0x18].r = 0x8B;
	pal[0x18].g = 0x73;
	pal[0x18].b = 0x00;

	pal[0x19].r = 0x00;
	pal[0x19].g = 0x97;
	pal[0x19].b = 0x00;

	pal[0x1A].r = 0x00;
	pal[0x1A].g = 0xAB;
	pal[0x1A].b = 0x00;

	pal[0x1B].r = 0x00;
	pal[0x1B].g = 0x93;
	pal[0x1B].b = 0x3B;

	pal[0x1C].r = 0x00;
	pal[0x1C].g = 0x83;
	pal[0x1C].b = 0x8B;

	pal[0x1D].r = 0x00;
	pal[0x1D].g = 0x00;
	pal[0x1D].b = 0x00;

	pal[0x1E].r = 0x00;
	pal[0x1E].g = 0x00;
	pal[0x1E].b = 0x00;

	pal[0x1F].r = 0x00;
	pal[0x1F].g = 0x00;
	pal[0x1F].b = 0x00;

	pal[0x20].r = 0xFF;
	pal[0x20].g = 0xFF;
	pal[0x20].b = 0xFF;

	pal[0x21].r = 0x3F;
	pal[0x21].g = 0xBF;
	pal[0x21].b = 0xFF;

	pal[0x22].r = 0x57;
	pal[0x22].g = 0x9F;
	pal[0x22].b = 0xFF;

	pal[0x23].r = 0xA7;
	pal[0x23].g = 0x8B;
	pal[0x23].b = 0xFD;

	pal[0x24].r = 0xF7;
	pal[0x24].g = 0x7B;
	pal[0x24].b = 0xFF;

	pal[0x25].r = 0xFF;
	pal[0x25].g = 0x77;
	pal[0x25].b = 0xB7;

	pal[0x26].r = 0xFF;
	pal[0x26].g = 0x77;
	pal[0x26].b = 0x63;

	pal[0x27].r = 0xFF;
	pal[0x27].g = 0x9B;
	pal[0x27].b = 0x3B;

	pal[0x28].r = 0xF3;
	pal[0x28].g = 0xBF;
	pal[0x28].b = 0x3F;

	pal[0x29].r = 0x83;
	pal[0x29].g = 0xD3;
	pal[0x29].b = 0x13;

	pal[0x2A].r = 0x4F;
	pal[0x2A].g = 0xDF;
	pal[0x2A].b = 0x4B;

	pal[0x2B].r = 0x58;
	pal[0x2B].g = 0xF8;
	pal[0x2B].b = 0x98;

	pal[0x2C].r = 0x00;
	pal[0x2C].g = 0xEB;
	pal[0x2C].b = 0xDB;

	pal[0x2D].r = 0x00;
	pal[0x2D].g = 0x00;
	pal[0x2D].b = 0x00;

	pal[0x2E].r = 0x00;
	pal[0x2E].g = 0x00;
	pal[0x2E].b = 0x00;

	pal[0x2F].r = 0x00;
	pal[0x2F].g = 0x00;
	pal[0x2F].b = 0x00;

	pal[0x30].r = 0xFF;
	pal[0x30].g = 0xFF;
	pal[0x30].b = 0xFF;

	pal[0x31].r = 0xAB;
	pal[0x31].g = 0xE7;
	pal[0x31].b = 0xFF;

	pal[0x32].r = 0xC7;
	pal[0x32].g = 0xD7;
	pal[0x32].b = 0xFF;

	pal[0x33].r = 0xD7;
	pal[0x33].g = 0xCB;
	pal[0x33].b = 0xFF;

	pal[0x34].r = 0xFF;
	pal[0x34].g = 0xC7;
	pal[0x34].b = 0xFF;

	pal[0x35].r = 0xFF;
	pal[0x35].g = 0xC7;
	pal[0x35].b = 0xDB;

	pal[0x36].r = 0xFF;
	pal[0x36].g = 0xBF;
	pal[0x36].b = 0xB3;

	pal[0x37].r = 0xFF;
	pal[0x37].g = 0xDB;
	pal[0x37].b = 0xAB;

	pal[0x38].r = 0xFF;
	pal[0x38].g = 0xE7;
	pal[0x38].b = 0xA3;

	pal[0x39].r = 0xE3;
	pal[0x39].g = 0xFF;
	pal[0x39].b = 0xA3;

	pal[0x3A].r = 0xAB;
	pal[0x3A].g = 0xF3;
	pal[0x3A].b = 0xBF;

	pal[0x3B].r = 0xB3;
	pal[0x3B].g = 0xFF;
	pal[0x3B].b = 0xCF;

	pal[0x3C].r = 0x9F;
	pal[0x3C].g = 0xFF;
	pal[0x3C].b = 0xF3;

	pal[0x3D].r = 0x00;
	pal[0x3D].g = 0x00;
	pal[0x3D].b = 0x00;

	pal[0x3E].r = 0x00;
	pal[0x3E].g = 0x00;
	pal[0x3E].b = 0x00;

	pal[0x3F].r = 0x00;
	pal[0x3F].g = 0x00;
	pal[0x3F].b = 0x00;
}

NES_PPU::~NES_PPU()
{
	delete[] real_nt1;
	delete[] real_nt2;
}

void NES_PPU::set_mapper(NES_Mapper *_mapper)
{
	mapper = _mapper;
}

void NES_PPU::unset_mapper()
{
	mapper = NULL;
}

void NES_PPU::set_cpu(P2A03* _cpu)
{
	cpu = _cpu;
}

void NES_PPU::unset_cpu()
{
	cpu = NULL;
}

void NES_PPU::set_screen(SDL_Surface *_screen)
{
	screen = _screen;
}

//TODO : gestion des miroirs (adresses >= $4000)
uint8_t NES_PPU::read_m(NES_Memory_Space space, uint16_t addr)
{
	uint8_t tmpbyte = buffer;

	if(space == NES_MEMORY_PRG) //PPU Registers
	{
		if(addr >= 0x2000 && addr <= 0x3FFF)
		{
			addr = ((addr - 0x2000) % 0x8) + 0x2000;
			if(addr == 0x2000)
			{
				return reg_2000;
			}
			else if(addr == 0x2001)
			{
				return reg_2001;
			}
			else if(addr == 0x2002)
			{
				//TODO correctly
				bool tmp = is_vblank_2002;
				is_vblank_2002 = false;
				VRAM_latch = LATCH_WRITE1;
				return (tmp ? 1 << 7 : 0) | (sprite_0_hit << 6) | (sprite_overflow << 5);
			}
			else if(addr == 0x2004)
			{
				if(!is_vblank)
					reg_2003++;
				return SPR_RAM[reg_2003];
			}
			else if(addr == 0x2007)
			{
				uint8_t tmp = read_m(NES_MEMORY_CHR, reg_2006);
				reg_2006 += ((reg_2000 >> 2) & 1) == 0 ? 1 : 32;
				return tmp;
			}
			else
			{
				return 0;
			}
		}
		//else : error
	}
	else if(space == NES_MEMORY_CHR)
	{
		addr = addr % 0x4000;

		if(addr <= 0x1FFF)
		{
			buffer = mapper->read_m(space, addr);
			return tmpbyte;
		}
		else if(addr <= 0x3EFF)
		{
			if(addr >= 0x3000)
				addr -= 0x1000;

			uint8_t nt_digit = (addr >> 8) & 0xF;
			if(nt_digit <= 3)
			{
				buffer = nt1[addr - 0x2000];
				return tmpbyte;
			}
			else if(nt_digit <= 7)
			{
				buffer = nt2[addr - 0x2400];
				return tmpbyte;
			}
			else if(nt_digit <= 0xB)
			{
				buffer = nt3[addr - 0x2800];
				return tmpbyte;
			}
			else if(nt_digit <= 0xF)
			{
				buffer = nt4[addr - 0x2C00];
				return tmpbyte;
			}
		}
		else
		{
			uint8_t pal_addr = (addr - 0x3F00) % 0x20;

			if(addr <= 0xF)
			{
				buffer = nt4[addr - 0x1000 - 0x2C00];
				return bg_pal[pal_addr];
			}
			else
			{
				buffer = nt4[addr - 0x1000 - 0x2C00];
				return spr_pal[pal_addr - 0x10];
			}
		}
	}

	return 0;
}

//TODO : gestion des miroirs (adresses >= $4000)
void NES_PPU::write_m(NES_Memory_Space space, uint16_t addr, uint8_t val)
{
	if(space == NES_MEMORY_PRG) //PPU Registers
	{
		if(addr >= 0x2000 && addr <= 0x3FFF)
		{
			addr = ((addr - 0x2000) % 0x8) + 0x2000;

			if(addr == 0x2000)
			{
				reg_2000 = val;
			}
			else if(addr == 0x2001)
			{
				reg_2001 = val;
			}
			else if(addr == 0x2003)
			{
				reg_2003 = val;
			}
			else if(addr == 0x2004)
			{
				SPR_RAM[reg_2003] = val;
				reg_2003++;
			}
			else if(addr == 0x2005)
			{
				if(VRAM_latch == LATCH_WRITE1)
				{
					VRAM_latch = LATCH_WRITE2;
					x_scroll = val;
				}
				else
				{
					VRAM_latch = LATCH_WRITE1;
					 y_scroll = val;
				}
			}
			else if(addr == 0x2006)
			{
				if(VRAM_latch == LATCH_WRITE1)
				{
					VRAM_latch = LATCH_WRITE2;
					reg_2006 = (reg_2006 & 0xFF) | (val << 8); 
				}
				else
				{
					VRAM_latch = LATCH_WRITE1;
					reg_2006 = (reg_2006 & 0xFF00) | val;
				}
			}
			else if(addr == 0x2007)
			{
				this->write_m(NES_MEMORY_CHR, reg_2006, val);
				reg_2006 += ((reg_2000 >> 2) & 1) == 0 ? 1 : 32;
			}
		}
		else if(addr == 0x4014)
		{
			//Note : time is not taken into account but using DMA
			//stop the program execution to do the copy, process
			//taking 513 cycles

			uint16_t page = val;
			uint8_t byte_to_write;

			for(unsigned int i = 0 ; i < 256 ; i++)
			{
				byte_to_write = cpu->read_m(NES_MEMORY_PRG, page * 0x100 + i);
				this->write_m(NES_MEMORY_PRG, 0x2004, byte_to_write);
			}
		}
	}
	else if(space == NES_MEMORY_CHR)
	{
		addr = addr % 0x4000;

		if(addr <= 0x1FFF)
		{
			mapper->write_m(space, addr, val);
		}
		else if(addr <= 0x3EFF)
		{
			if(addr >= 0x3000)
				addr -= 0x1000;

			uint8_t nt_digit = (addr >> 8) & 0xF;
			if(nt_digit <= 3)
			{
				nt1[addr - 0x2000] = val;
			}
			else if(nt_digit <= 7)
			{
				nt2[addr - 0x2400] = val;
			}
			else if(nt_digit <= 0xB)
			{
				nt3[addr - 0x2800] = val;
			}
			else if(nt_digit <= 0xF)
			{
				nt4[addr - 0x2C00] = val;
			}		
		}
		else
		{
			addr = (addr - 0x3F00) % 0x20;

			if(addr == 0x0 || addr == 0x10)
			{
				bg_pal[0] = spr_pal[0] = val;
			}
			else if(addr <= 0xF)
				bg_pal[addr] = val;
			else
				spr_pal[addr - 0x10] = val;
		}
	}
}

void NES_PPU::make_powerup_state()
{
	reg_2000 = 0;
	reg_2001 = 0;
	reg_2003 = 0;
	reg_2006 = 0;

	VRAM_latch = LATCH_WRITE1;
	x_scroll = y_scroll = 0;

	cur_scanline = 241;
	is_vblank = is_vblank_2002 = true;
}

void NES_PPU::make_reset_state()
{
	reg_2000 = 0;
	reg_2001 = 0;
}

void NES_PPU::switch_mirroring(NES_Mirroring _mirroring)
{
	mirroring = _mirroring;

	switch(mirroring)
	{
		case NES_MIRRORING_H:
			nt1 = nt2 = real_nt1;
			nt3 = nt4 = real_nt2;
			break;
		case NES_MIRRORING_V:
			nt1 = nt3 = real_nt1;
			nt2 = nt4 = real_nt2;
			break;
		case NES_MIRRORING_1SA:
			nt1 = nt2 = nt3 = nt4 = real_nt1;
			break;
		case NES_MIRRORING_1SB:
			nt1 = nt2 = nt3 = nt4 = real_nt2;
			break;
	}
}

unsigned int NES_PPU::get_cur_scanline() const
{
	return cur_scanline;
}

void NES_PPU::inc_cur_scanline()
{
	cur_scanline++;
}

void NES_PPU::render_scanline()
{	
	uint8_t bg_nt_nb = reg_2000 & 3; //Number of the name table used for the backgroud
	uint8_t *bg_nt = bg_nt_nb == 0 ? nt1 : bg_nt_nb == 1 ? nt2 : bg_nt_nb == 2 ? nt3 : nt4; //Name table used for backgroud display

	uint16_t cur_x = x_scroll;
	uint16_t cur_y = (cur_scanline - 1 + y_scroll);

	uint16_t nt_offset = ((cur_y % 240) / 8) * 32 + cur_x / 8; //Offset of the current tile from the beginning of the Name Table 
	uint16_t at_offset = ((cur_y % 240) / 32) * 8 + cur_x / 32; //Offset of the current tile from the beginning of the Attribute Table

	//Put those variables in the PPU classes and only update them when the user write in 0x2000 ?
	uint8_t bg_pt = (reg_2000 >> 4) & 1; //Pattern Table for the background
	uint8_t spr_pt = (reg_2000 >> 3) & 1; //Pattern Table for the sprites

	printf("\nx_scroll: %u, y_scroll: %u\n", x_scroll, y_scroll);

	if(cur_y >= 240)
	{
		cur_y %= 240;

		if(bg_nt == nt1)
			bg_nt = nt3;
		else if(bg_nt == nt2)
			bg_nt = nt4;
		else if(bg_nt == nt3)
			bg_nt = nt1;
		else
			bg_nt = nt4;

		nt_offset = (cur_y / 8) * 32 + cur_x / 8;
		at_offset = (cur_y / 32) * 8 + cur_x / 32;
	}

	if(cur_scanline == 0)
	{
		is_vblank = false;
		sprite_0_hit = 0; //set to 0 a dot 1 of the pre render line
		sprite_overflow = 0;
	}
	else if(cur_scanline >= 1 && cur_scanline <= 240)
	{
		uint16_t cur_tile = bg_nt[nt_offset];
		uint16_t cur_at_byte = bg_nt[960 + at_offset];
		uint8_t bg_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * bg_pt + 0x10 * cur_tile + cur_y % 8);
		uint8_t bg_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * bg_pt + 0x10 * cur_tile + cur_y % 8 + 8);
		uint8_t color;

		uint8_t spr_ram_2_cur = 0;

		bool sprite0 = false;

		uint8_t background[256] = {0}, spr_fg[256] = {0}, spr_bg[256] = {0};
		Color scanline[256];

			//Sprites : loading secondary OAM (SPR_RAM_2)
			//http://wiki.nesdev.com/w/index.php/PPU_sprite_evaluation
			for(int i = 0 ; i < 8 * 4 ; i++)
				SPR_RAM_2[i] = 0xFF; //SEC OAM initialized to $FF

			//We try to fill the whole SPR RAM 2 with the 64 sprites
			//from the SPR RAM
			if(reg_2000 >> 5 & 1 != 0) //8x16 sprites
			{
				for(unsigned int n = 0 ; n < 64 && spr_ram_2_cur < 9 ; n++)
				{
					if(SPR_RAM[n * 4] <= cur_scanline - 1 && SPR_RAM[n * 4] + 15 >= cur_scanline - 1)
					{
						if(n == 0)
							sprite0 = true;

						if(spr_ram_2_cur != 8)
						{
							for(int i = 0 ; i < 4 ; i++)
							{
								SPR_RAM_2[spr_ram_2_cur * 4 + i] = SPR_RAM[n * 4 + i];
							}

							spr_ram_2_cur++;
						}
						else
						{
							if(!(reg_2001 >> 3 == 0 && reg_2001 >> 4 == 0))
								sprite_overflow = 1;
						}
					}
				}
			}
			else //8x8 sprites
			{
				for(unsigned int n = 0 ; n < 64 && spr_ram_2_cur < 9 ; n++)
				{
					if(SPR_RAM[n * 4] <= cur_scanline - 1 && SPR_RAM[n * 4] + 7 >= cur_scanline - 1)
					{
						if(n == 0)
							sprite0 = true;

						if(spr_ram_2_cur != 8)
						{
							for(int i = 0 ; i < 4 ; i++)
							{
								SPR_RAM_2[spr_ram_2_cur * 4 + i] = SPR_RAM[n * 4 + i];
							}
						}
						else
						{
							if(!(reg_2001 >> 3 == 0 && reg_2001 >> 4 == 0))
								sprite_overflow = 1;
						}

						spr_ram_2_cur++;
					}
				}
			}

		//No sprite 0 hit when background or sprite rendering is off
		if(!((reg_2001 >> 3 & 1) && (reg_2001 >> 4 & 1)))
			sprite0 = false;

		for(int x = 0 ; x < 256 ; x++, cur_x++)
		{
			if(cur_x == 256)
			{
				if(bg_nt == nt1)
					bg_nt = nt2;
				else if(bg_nt == nt2)
					bg_nt = nt1;
				else if(bg_nt == nt3)
					bg_nt = nt4;
				else
					bg_nt = nt3;

				cur_x = 0;

				nt_offset = (cur_y / 8) * 32; //Offset of the current tile from the beginning of the Name Table 
				at_offset = (cur_y / 32) * 8; //Offset of the current tile from the beginning of the Attribute Table

				cur_tile = bg_nt[nt_offset]; //Number of the tile currently procedeed
				cur_at_byte = bg_nt[960 + at_offset];
				bg_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * bg_pt + 0x10 * cur_tile + cur_y % 8);
				bg_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * bg_pt + 0x10 * cur_tile + cur_y % 8 + 8);
			}

			if(cur_x % 8 == 0 && x != 0)
			{
				//For each tile, the PPU fetch the NT byte, then the AT byte then the 2 PT bytes

				if(cur_x != 0)
					nt_offset++;
				cur_tile = bg_nt[nt_offset];

				//0x10 = 16 bytes per tile
				bg_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * bg_pt + 0x10 * cur_tile + cur_y % 8);
				bg_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * bg_pt + 0x10 * cur_tile + cur_y % 8 + 8);
			}

			if(cur_x % 32 == 0 && x != 0)
			{
				if(cur_x != 0)
					at_offset++;
				cur_at_byte = bg_nt[960 + at_offset];
			}

			color = (bg_pt_byte1 >> (7 - cur_x % 8)) & 1;
			color |= ((bg_pt_byte2 >> (7 - cur_x % 8)) & 1) << 1;
			if((cur_y % 32) / 16 == 0)
			{
				if((cur_x % 32) / 16 == 0)
				{
					color |= (cur_at_byte & 3) << 2;
				}
				else
				{
					color |= ((cur_at_byte >> 2) & 3) << 2;
				}
			}
			else
			{
				if((cur_x % 32) / 16 == 0)
				{
					color |= ((cur_at_byte >> 4) & 3) << 2;
				}
				else
				{
					color |= ((cur_at_byte >> 6) & 3) << 2;
				}
			}

			background[x] = color;
		}

		//(Flawed) Sprite rendering
		for(int x = 0 ; x < spr_ram_2_cur ; x++)
		{
			uint8_t spr_pt_byte1, spr_pt_byte2;
			uint8_t spr_color;

			if(SPR_RAM_2[x*4 + 2] >> 7) //Flip vertically
			{
				if(reg_2000 >> 5 & 1 != 0) //8x16 sprites
				{
					if(((cur_scanline - 1) - SPR_RAM_2[x*4]) < 8)
					{
						spr_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * (SPR_RAM_2[x * 4 + 1] & 1) + 0x10 * ((SPR_RAM_2[x * 4 + 1] & 0xFE) + 1) + 7 - ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8);
						spr_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * (SPR_RAM_2[x * 4 + 1] & 1)  + 0x10 * ((SPR_RAM_2[x * 4 + 1] & 0xFE) + 1) + 7 - ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8 + 8);
					}
					else
					{
						spr_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * (SPR_RAM_2[x * 4 + 1] & 1)  + 0x10 * (SPR_RAM_2[x * 4 + 1] & 0xFE) + 7 - ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8);
						spr_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * (SPR_RAM_2[x * 4 + 1] & 1)  + 0x10 * (SPR_RAM_2[x * 4 + 1] & 0xFE) + 7 - ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8 + 8);
					}
				}
				else
				{
					spr_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * spr_pt + 0x10 * SPR_RAM_2[x * 4 + 1] + 7 - ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8);
					spr_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * spr_pt + 0x10 * SPR_RAM_2[x * 4 + 1] + 7 - ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8 + 8);
				}
			}
			else //Do not flip vertically
			{
				if(reg_2000 >> 5 & 1 != 0) //8x16 sprites
				{
					if(((cur_scanline - 1) - SPR_RAM_2[x*4]) < 8)
					{
						spr_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * (SPR_RAM_2[x * 4 + 1] & 1) + 0x10 * (SPR_RAM_2[x * 4 + 1] & 0xFE) + ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8);
						spr_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * (SPR_RAM_2[x * 4 + 1] & 1)  + 0x10 * (SPR_RAM_2[x * 4 + 1] & 0xFE) + ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8 + 8);
					}
					else
					{
						spr_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * (SPR_RAM_2[x * 4 + 1] & 1)  + 0x10 * ((SPR_RAM_2[x * 4 + 1] & 0xFE) + 1) + ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8);
						spr_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * (SPR_RAM_2[x * 4 + 1] & 1)  + 0x10 * ((SPR_RAM_2[x * 4 + 1] & 0xFE) + 1) + ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8 + 8);
					}
				}
				else
				{
					spr_pt_byte1 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * spr_pt + 0x10 * SPR_RAM_2[x * 4 + 1] + ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8);
					spr_pt_byte2 = mapper->read_m(NES_MEMORY_CHR, 0x1000 * spr_pt + 0x10 * SPR_RAM_2[x * 4 + 1] + ((cur_scanline - 1) - SPR_RAM_2[x*4]) % 8 + 8);
				}
			}

			uint8_t color_byte1 = (SPR_RAM_2[x * 4 + 2] & 3) << 2;

			if(SPR_RAM_2[x * 4 + 2] >> 5 & 1)
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if((SPR_RAM_2[x*4 + 2] >> 6) & 1)
					{
						uint8_t color = color_byte1 |
							((spr_pt_byte1 >> (7 - j)) & 1) |
							(((spr_pt_byte2 >> (7 - j)) & 1) << 1);

						if((color & 3) != 0)
						{
							spr_bg[SPR_RAM_2[x * 4 + 3] + 7 - j] = color;

							if(x == 0 && sprite0 == true && (color & 3) != 0 && (background[SPR_RAM_2[x * 4 + 3] + 7 - j] & 3) != 0)
								sprite_0_hit = 1;
						}
					}
					else
					{
						uint8_t color = color_byte1 |
							((spr_pt_byte1 >> (7 - j)) & 1) |
							(((spr_pt_byte2 >> (7 - j)) & 1) << 1);

						if((color & 3) != 0)
						{
							spr_bg[SPR_RAM_2[x * 4 + 3] + j] = color;

							if(x == 0 && sprite0 == true && (color & 3) != 0 && (background[SPR_RAM_2[x * 4 + 3] + j] & 3) != 0)
								sprite_0_hit = 1;
						}	
					}
				}
			}
			else
			{
				for(int j = 0 ; j < 8 ; j++)
				{
					if((SPR_RAM_2[x*4 + 2] >> 6) & 1)
					{
						uint8_t color = color_byte1 |
							((spr_pt_byte1 >> (7 - j)) & 1) |
							(((spr_pt_byte2 >> (7 - j)) & 1) << 1);

						if((color & 3) != 0)
						{
							spr_fg[SPR_RAM_2[x * 4 + 3] + 7 - j] = color;

							if(x == 0 && sprite0 == true && (color & 3) != 0 && (background[SPR_RAM_2[x * 4 + 3] + 7 - j] & 3) != 0)
								sprite_0_hit = 1;
						}	
					}
					else
					{
						uint8_t color = color_byte1 |
							((spr_pt_byte1 >> (7 - j)) & 1) |
							(((spr_pt_byte2 >> (7 - j)) & 1) << 1);

						if((color & 3) != 0)
						{
							spr_fg[SPR_RAM_2[x * 4 + 3] + j] = color;

							if(x == 0 && sprite0 == true && (color & 3) != 0 && (background[SPR_RAM_2[x * 4 + 3] + j] & 3) != 0)
								sprite_0_hit = 1;
						}	
					}
				}
			}
		}

		//Merging background and sprites
		for(int x = 0 ; x < 256 ; x++)
		{
			if(spr_fg[x] == 0 || spr_fg[x] == 4 || spr_fg[x] == 8 || spr_fg[x] == 12 || ((reg_2001 >> 4) & 1) == 0)
			{
				if(background[x] == 0 || background[x] == 4 || background[x] == 8 || background[x] == 12 || ((reg_2001 >> 3) & 1) == 0)
				{
					if((reg_2001 >> 4) & 1)
						scanline[x] = pal[spr_pal[spr_bg[x]]];
					else
					{
						scanline[x].r = 0;
						scanline[x].g = 0;
						scanline[x].b = 0;
					}
				}
				else
				{
					if((reg_2001 >> 3) & 1)
						scanline[x] = pal[bg_pal[background[x]]];
					else
					{
						scanline[x].r = 0;
						scanline[x].g = 0;
						scanline[x].b = 0;
					}
				}
			}
			else
			{
				scanline[x] = pal[spr_pal[spr_fg[x]]];
			}
		}

		//Actual drawing on screen (SDL surface)
		SDL_LockSurface(screen);
		for(int x = 0 ; x < 256 ; x++)
		{
			putPixel(screen, x, cur_scanline - 1, scanline[x], multiple);
		}
		SDL_UnlockSurface(screen);
	}
	else if(cur_scanline == 241)
	{
		is_vblank = is_vblank_2002 = true;
		if(reg_2000 >> 7)
			cpu->start_interrupt(NES_INTERRUPT_NMI);
	}

	cur_scanline++;

	if(cur_scanline >= 262)
		cur_scanline = 0;
}

void NES_PPU::load_ini(Ini &ini)
{
	multiple = atoi(ini.get("General", "DisplayMultiple").c_str());
	if(multiple < 1)
		multiple = 1;
	if(multiple > 4)
		multiple = 4;
}

void putPixel(SDL_Surface *surface, int x, int y, Color color, int multiple)
{
	if(multiple == 1)	
		*(uint32_t*) ((uint8_t*)surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel) = SDL_MapRGB(surface->format, color.r, color.g, color.b);
	else
	{
		SDL_Rect rect;
		rect.x = x*multiple;
		rect.y = y*multiple;
		rect.w = multiple;
		rect.h = multiple;
		SDL_FillRect(surface, &rect, SDL_MapRGB(surface->format, color.r, color.g, color.b));
	}
}
