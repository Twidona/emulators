#include <Error.hpp>

int NES_error = NES_NO_ERROR;

int NES_get_error()
{
	return NES_error;
}

std::string NES_get_error_string()
{
	switch(NES_error)
	{
		case NES_NO_ERROR:
			return "No error";
		case NES_ROM_LOAD_ERROR:
			return "[ROM] Loading error";
		case NES_ROM_WRONG_FORMAT:
			return "[ROM] Wrong file format";
		case NES_ROM_ALLOC_FAILURE:
			return "[ROM] Error allocating memory";
		case NES_MAPPER_ALLOC_FAILURE:	
			return "[Mapper] Error allocating memory";
		case NES_MAPPER_INCOMPATIBLE_INFO:
			return "[Mapper] Conflict between mapper number and ROM information";
		case NES_MAPPER_NOT_IMPLEMENTED:
			return "[Mapper] Mapper not implemented";
		case NES_MAPPER_BAD_READING:
			return "[Mapper] Read Error";
		case NES_CPU_BAD_READING:
			return "[CPU] Read Error";
		default:
			return "Unknown error";
	}
}
