#ifndef _EVENTS_HPP
#define _EVENTS_HPP

#include <stdint.h>

class Events
{
	public:
		Events();
		~Events();
		void poll();
		uint8_t wait();

		bool is_pressed(uint8_t key);
		bool is_quitev();

	private:
		uint8_t keys[0x10];
		bool quitev;
};

#endif
