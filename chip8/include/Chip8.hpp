#ifndef _CHIP8_HPP
#define _CHIP8_HPP

#include <iostream>
#include <string>
#include <stdint.h>

#include <SDL/SDL.h>

#include <Timer.hpp>
#include <Events.hpp>
#include <Ini.hpp>
#include <Color.hpp>

class Chip8
{
	public:
		Chip8();
		~Chip8();
		void reset();
		void clear_screen();
		void refresh_screen(SDL_Surface *sdl_screen);

		void load_font();
		bool load_rom(std::string path); //Return true on success
		bool load_conf();

		bool is_quitev();

		void next_iter();
		void execute_next_opcode();
		void execute_opcode(uint16_t opcode);

		int get_multiple() const;
		int get_freq() const;

	private:
		//private methods
		void opc_RET();
		void opc_JP(uint16_t addr);
		void opc_CALL(uint16_t addr);
		void opc_SE_Vx_byte(uint8_t reg_num, uint8_t val);
		void opc_SNE_Vx_byte(uint8_t reg_num, uint8_t val);
		void opc_SE_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_LD_Vx_byte(uint8_t reg, uint8_t val);
		void opc_ADD_Vx_byte(uint8_t reg, uint8_t val);
		void opc_LD_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_OR_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_AND_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_XOR_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_ADD_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_SUB_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_SHR_Vx(uint8_t reg);
		void opc_SUBN_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_SHL_Vx(uint8_t reg);
		void opc_SNE_Vx_Vy(uint8_t reg1, uint8_t reg2);
		void opc_LD_I_addr(uint16_t addr);
		void opc_JP_V0_byte(uint16_t val);
		void opc_RND(uint8_t reg, uint8_t val);
		void opc_DRW(uint8_t reg1, uint8_t reg2, uint8_t nbytes);
		void opc_SKP(uint8_t reg);
		void opc_SKNP(uint8_t reg);
		void opc_LD_Vx_DT(uint8_t reg);
		void opc_LD_Vx_K(uint8_t reg);
		void opc_LD_DT_Vx(uint8_t reg);
		void opc_LD_ST_Vx(uint8_t reg);
		void opc_ADD_I_Vx(uint8_t reg);
		void opc_LD_F_Vx(uint8_t reg);
		void opc_LD_B_Vx(uint8_t reg);
		void opc_LD_at_I_from_V0_to_Vx(uint8_t reg);
		void opc_LD_at_V0_to_Vx_from_I(uint8_t reg);

		//private attributes
		uint8_t mem[0x1000];

		//registers, "pseudo-registers" and the stack
		uint8_t V[0x10];
		uint16_t I;
		uint16_t PC; //Program Counter

		uint8_t DT; //Delay Timer
		uint8_t ST; //Sound Timer

		uint8_t SP; //Stack Pointer
		uint16_t stack[0x10];

		//32x64 screen
		uint8_t screen[32][64];

		Timer ST_DT_timer; //Timer for ST and DT registers

		Events events;
		bool waiting_key;
		uint8_t waiting_key_reg;

		//configuration
		int multiple;
		int freq;
		Color white, black;
};

#endif
