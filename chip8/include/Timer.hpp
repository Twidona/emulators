#ifndef _TIMER_HPP
#define _TIMER_HPP

#include <SDL/SDL.h>

//Timer class uses the SDL_GetTicks function.
//Because this function uses ms, it doesn't allow frequencies > 1000Hz
//So it will be changed in the future
//Still, should be sufficient for Chip 8
class Timer
{
	public:
		Timer(uint32_t freq); //in Hz
		~Timer();

		void start();
		bool next();

	private:
		uint16_t ms_p; //Timer period in ms
		Uint32 last_time, current_time;
};

#endif
