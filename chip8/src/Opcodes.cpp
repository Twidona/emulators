#include <iomanip>
#include <iostream>
#include <cstdio>

#include <Chip8.hpp>

using namespace std;

void Chip8::opc_RET()
{
	#ifdef DEBUG
	printf("RET\n");
	#endif

	if(SP > 0x0)
	{
		SP--;
		PC = stack[SP];
	}
	else
		fprintf(stderr, "Error[RET]: empty stack");
}

void Chip8::opc_JP(uint16_t addr)
{
	#ifdef DEBUG
	printf("JP 0x%.3X\n", addr);
	#endif
	PC = addr;
}

void Chip8::opc_CALL(uint16_t addr)
{
	#ifdef DEBUG
	printf("CALL 0x%.3X\n", addr);
	#endif

	if(SP < 0x10)
	{
		stack[SP] = PC;
		SP++;
		PC = addr;
	}
	else
		fprintf(stderr, "Error[CALL]: stack overflow\n");
}

void Chip8::opc_SE_Vx_byte(uint8_t reg_num, uint8_t val)
{
	#ifdef DEBUG
	printf("SE V%.1X, 0x%.2X\n", reg_num, val);
	#endif

	if(V[reg_num] == val)
		PC += 2;
}

void Chip8::opc_SNE_Vx_byte(uint8_t reg, uint8_t val)
{
	#ifdef DEBUG
	printf("SNE V%.1X, 0x%.2X\n", reg, val);
	#endif

	if(V[reg] != val)
		PC += 2;
}

void Chip8::opc_SE_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("SE V%.1X, V%.1X\n", reg1, reg2);
	#endif

	if(V[reg1] == V[reg2])
		PC += 2;
}

void Chip8::opc_LD_Vx_byte(uint8_t reg, uint8_t val)
{
	#ifdef DEBUG
	printf("LD V%.1X, 0x%.2X\n", reg, val);
	#endif

	V[reg] = val;
}

void Chip8::opc_ADD_Vx_byte(uint8_t reg, uint8_t val)
{
	#ifdef DEBUG
	printf("ADD V%.1X, 0x%.2X\n", reg, val);
	#endif

	V[reg] += val;
}

void Chip8::opc_LD_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("LD V%.1X, V%.1X\n", reg1, reg2);
	#endif

	V[reg1] = V[reg2];
}

void Chip8::opc_OR_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("OR V%.1X, V%.1X\n", reg1, reg2);
	#endif

	V[reg1] |= V[reg2];
}

void Chip8::opc_AND_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("AND V%.1X, V%.1X\n", reg1, reg2);
	#endif

	V[reg1] &= V[reg2];
}

void Chip8::opc_XOR_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("XOR V%.1X, V%.1X\n", reg1, reg2);
	#endif

	V[reg1] ^= V[reg2];
}

void Chip8::opc_ADD_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("ADD V%.1X, V%.1X\n", reg1, reg2);
	#endif

	if(V[reg1] + V[reg2] > 0xFF)
		V[0xF] = 1;
	else
		V[0xF] = 0;

	V[reg1] += V[reg2];
}

void Chip8::opc_SUB_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("SUB V%.1X, V%.1X\n", reg1, reg2);
	#endif

	if(V[reg1] > V[reg2])
		V[0xF] = 1;
	else
		V[0xF] = 0;

	V[reg1] -= V[reg2];
}

void Chip8::opc_SHR_Vx(uint8_t reg)
{
	#ifdef DEBUG
	printf("SHR V%.1X\n", reg);
	#endif

	V[0xF] = V[reg] & 0x1;
	V[reg] >>= 1;
}

void Chip8::opc_SUBN_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("SUBN V%.1X, V%.1X\n", reg1, reg2);
	#endif

	if(V[reg2] > V[reg1])
		V[0xF] = 1;
	else
		V[0xF] = 0;

	V[reg1] = V[reg2] - V[reg1];
}

void Chip8::opc_SHL_Vx(uint8_t reg)
{
	#ifdef DEBUG
	printf("SHL V%.1X\n", reg);
	#endif

	V[0xF] = V[reg] & 0x80 ? 1 : 0;
	V[reg] <<= 1;
}

void Chip8::opc_SNE_Vx_Vy(uint8_t reg1, uint8_t reg2)
{
	#ifdef DEBUG
	printf("SNE V%.1X, V%.1X\n", reg1, reg2);
	#endif

	if(V[reg1] != V[reg2])
		PC += 2;
}

void Chip8::opc_LD_I_addr(uint16_t addr)
{
	#ifdef DEBUG
	printf("LD I, 0x%.3X\n", addr);
	#endif

	I = addr;
}

void Chip8::opc_JP_V0_byte(uint16_t val)
{
	#ifdef DEBUG
	printf("JP V0, 0x%.3X\n", val);
	#endif

	PC = V[0] + val;
}

void Chip8::opc_RND(uint8_t reg, uint8_t val)
{
	#ifdef DEBUG
	printf("RND V%.1X, 0x%.2X\n", reg, val);
	#endif

	V[reg] = (rand() % 0x100) & val;
}

void Chip8::opc_DRW(uint8_t reg1, uint8_t reg2, uint8_t nbytes)
{
	#ifdef DEBUG
	printf("DRW V%.1X, V%.1X, 0x%.1X\n", reg1, reg2, nbytes);
	#endif

	int x = V[reg1], y = V[reg2];

	V[0xF] = 0;
	for(int i = 0 ; i < nbytes ; i++)
	{
		uint8_t cur_byte = mem[I + i];
		for(int j = 0 ; j < 8 ; j++)
		{
			uint8_t cur_bit = (cur_byte >> (7 - j)) & 1;

			//Check if the current pixel is set and will be unset
			if(cur_bit == 1 && screen[(y + i) % 32][(x + j) % 64] == 1)
				V[0xF] = 1;

			screen[(y + i) % 32][(x + j) % 64] ^= cur_bit;
		}
	}
}

void Chip8::opc_SKP(uint8_t reg)
{
	#ifdef DEBUG
	printf("SKP V%.1X\n", reg);
	#endif

	if(events.is_pressed(V[reg]))
		PC += 2;
}

void Chip8::opc_SKNP(uint8_t reg)
{
	#ifdef DEBUG
	printf("SKNP V%.1X\n", reg);
	#endif

	if(!events.is_pressed(V[reg]))
		PC += 2;
}

void Chip8::opc_LD_Vx_DT(uint8_t reg)
{
	#ifdef DEBUG
	printf("LD V%.1X, DT\n", reg);
	#endif

	V[reg] = DT;
}

void Chip8::opc_LD_Vx_K(uint8_t reg)
{
	#ifdef DEBUG
	printf("LD V%.1X, K\n", reg);
	#endif

	waiting_key = true;
	waiting_key_reg = reg;
}

void Chip8::opc_LD_DT_Vx(uint8_t reg)
{
	#ifdef DEBUG
	printf("LD DT, V%.1X\n", reg);
	#endif

	DT = V[reg];
}

void Chip8::opc_LD_ST_Vx(uint8_t reg)
{
	#ifdef DEBUG
	printf("LD ST, V%.1X\n", reg);
	#endif

	ST = V[reg];
}

void Chip8::opc_ADD_I_Vx(uint8_t reg)
{
	#ifdef DEBUG
	printf("ADD I, V%.1X\n", reg);
	#endif

	I += V[reg];
}

void Chip8::opc_LD_F_Vx(uint8_t reg)
{
	#ifdef DEBUG
	printf("LD F, V%.1X\n", reg);
	#endif

	I = 0x100 + 5 * (V[reg] & 0x0F);
}

void Chip8::opc_LD_B_Vx(uint8_t reg)
{
	#ifdef DEBUG
	printf("LD B, V%.1X\n", reg);
	#endif

	mem[I] = V[reg] / 100;
	mem[I + 1] = (V[reg] / 10) % 10;
	mem[I + 1] = V[reg] % 10;
}

void Chip8::opc_LD_at_I_from_V0_to_Vx(uint8_t reg)
{
	#ifdef DEBUG
	printf("LD [I], V%.1X\n", reg);
	#endif

	for(int i = 0 ; i < reg + 1 ; i++)
	{
		mem[I + i] = V[i];
	}
}

void Chip8::opc_LD_at_V0_to_Vx_from_I(uint8_t reg)
{
	#ifdef DEBUG
	printf("LD V%.1X, [I]\n", reg);
	#endif

	for(int i = 0 ; i < reg + 1 ; i++)
	{
		V[i] = mem[I + i];
	}
}
