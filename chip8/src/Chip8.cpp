#include <string>
#include <cstring>

#include <iomanip>
#include <iostream>
#include <cstdio>
#include <cstdlib>

#include <SDL/SDL.h>

#include <Chip8.hpp>
#include <Pixels.hpp>
#include <Timer.hpp>

using namespace std;

Chip8::Chip8() : ST_DT_timer(60), multiple(1)
{
	white.r = 255;
	white.g = 255;
	white.b = 255;

	black.r = 0;
	black.g = 0;
	black.b = 0;

	srand(time(NULL));
	reset();

	load_conf();
}

Chip8::~Chip8()
{

}

void Chip8::reset()
{
	memset((void*) mem, 0, 0x1000);
	memset((void*) V, 0, 0x10);
	I = ST = DT = SP = 0;
	PC = 0x200;
	clear_screen();
	load_font();
	ST_DT_timer.start();
	waiting_key = false;
}

void Chip8::clear_screen()
{
	memset((void*) screen, 0, 32*64);
}

void Chip8::refresh_screen(SDL_Surface *sdl_screen)
{
	SDL_LockSurface(sdl_screen);
	for(int i = 0 ; i < 32 ; i++)
	{
		for(int j = 0 ; j < 64 ; j++)
		{
			if(screen[i][j])
				putPixel(sdl_screen, j, i, white, multiple);
			else
				putPixel(sdl_screen, j, i, black, multiple);
		}
	}
	SDL_UnlockSurface(sdl_screen);
}

//Load Chip-8 font at 0x100 in memory (font ends at 0x150)
void Chip8::load_font()
{
	const uint8_t font[0x50] =
	{ 
		0xF0, 0x90, 0x90, 0x90, 0xF0, //0
		0x20, 0x60, 0x20, 0x20, 0x70, //1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
		0x90, 0x90, 0xF0, 0x10, 0x10, //4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
		0xF0, 0x10, 0x20, 0x40, 0x40, //7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
		0xF0, 0x90, 0xF0, 0x90, 0x90, //A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
		0xF0, 0x80, 0x80, 0x80, 0xF0, //C
		0xE0, 0x90, 0x90, 0x90, 0xE0, //D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
		0xF0, 0x80, 0xF0, 0x80, 0x80  //F
	};

	memcpy((void *) &(mem[0x100]), (const void *) font, 0x50);
}

bool Chip8::load_rom(string path)
{
	FILE *rom_file = NULL;
	long int rom_size = 0;

	rom_file = fopen(path.c_str(), "rb");
	if(!rom_file)
	{
		cout << "Error opening " << path << endl;
		return false;
	}

	//We get the rom file's size
	fseek(rom_file, 0, SEEK_END);
	rom_size = ftell(rom_file);
	fseek(rom_file, 0, SEEK_SET);

	//And load it in memory
	fread(&(mem[0x200]), 1, rom_size, rom_file);

	fclose(rom_file);
	return true;
}

Color parse_color(string colorstr, Color default_color)
{
	Color color = {0, 0, 0};
	int pos = 0;
	string sub = "";

	pos = colorstr.find_first_of(',');
	if(pos == string::npos)
		return default_color;
	sub = colorstr.substr(0, pos);
	colorstr = colorstr.substr(pos + 1);
	color.r = strtol(sub.c_str(), NULL, 10);

	pos = colorstr.find_first_of(',');
	if(pos == string::npos)
		return default_color;
	sub = colorstr.substr(0, pos);
	colorstr = colorstr.substr(pos + 1);
	color.g = strtol(sub.c_str(), NULL, 10);

	color.b = strtol(colorstr.c_str(), NULL, 10);

	return color;
}

bool Chip8::load_conf()
{
	Ini ini;
	IniMap inimap;

	Color white_ = {255, 255, 255}, black_ = {0, 0, 0};

	inimap["Display"]["multiple"] = "5";
	inimap["Colors"]["white"] = "255,255,255";
	inimap["Colors"]["black"] = "0,0,0";
	inimap["CPU"]["freq"] = "360";

	string home = getenv("HOME");
	if(!home.empty())
	{
		string path = home + "/.config/emulators/chip8.ini";
		ini.load(path);
	}

	ini.load_default(inimap);

	multiple = strtol(ini.get("Display", "multiple").c_str(), NULL, 10);
	if(multiple < 0)
		multiple = 5;

	white = parse_color(ini.get("Colors", "white"), white_);
	black = parse_color(ini.get("Colors", "black"), black_);

	freq = strtol(ini.get("CPU", "freq").c_str(), NULL, 10);
	if(freq < 0 || freq > 1000)
		freq = 360;
}

bool Chip8::is_quitev()
{
	return events.is_quitev();
}

void Chip8::next_iter()
{
	if(!waiting_key)
	{
		execute_next_opcode();
		events.poll();
	}
	else
	{
		uint8_t res = events.wait();
		if(res != 0x10)
		{
			V[waiting_key_reg] = res;
			waiting_key = false;
		}
	}

	if(ST_DT_timer.next())
	{
		if(ST > 0)
			ST--;

		if(DT > 0)
			DT--;
	}
}

void Chip8::execute_next_opcode()
{
	uint16_t opcode;

	opcode = (mem[PC] << 8) + mem[PC + 1];
	if(PC + 2 < 0x1000)
		PC += 2;

	execute_opcode(opcode);
}

void Chip8::execute_opcode(uint16_t opcode)
{
	switch(opcode & 0xF000)
	{
		case 0x0000:
			if(opcode == 0x00E0)
				clear_screen(); //CLS opcode
			else if(opcode == 0x00EE)
				opc_RET();
			else
				#ifdef DEBUG
					cout << "Unknown opcode 0x" << hex << setfill('0') << setw(4) << opcode;
				#endif
			break;

		case 0x1000:
			opc_JP(opcode & 0x0FFF);
			break;

		case 0x2000:
			opc_CALL(opcode & 0x0FFF);
			break;

		case 0x3000:
			opc_SE_Vx_byte((opcode & 0x0F00) >> 8, opcode & 0x00FF);
			break;

		case 0x4000:
			opc_SNE_Vx_byte((opcode & 0x0F00) >> 8, opcode & 0x00FF);
			break;

		case 0x5000:
			opc_SE_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
			break;

		case 0x6000:
			opc_LD_Vx_byte((opcode & 0x0F00) >> 8, opcode & 0x00FF);
			break;

		case 0x7000:
			opc_ADD_Vx_byte((opcode & 0x0F00) >> 8, opcode & 0x00FF);
			break;

		case 0x8000:
			switch(opcode & 0xF)
			{
				case 0x0:
					opc_LD_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
					break;

				case 0x1:
					opc_OR_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
					break;

				case 0x2:
					opc_AND_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
					break;

				case 0x3:
					opc_XOR_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
					break;

				case 0x4:
					opc_ADD_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
					break;

				case 0x5:
					opc_SUB_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
					break;

				case 0x6:
					opc_SHR_Vx((opcode & 0x0F00) >> 8);
					break;

				case 0x7:
					opc_SUBN_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
					break;

				case 0xE:
					opc_SHL_Vx((opcode & 0x0F00) >> 8);
					break;
			}
			break;

		case 0x9000:
			opc_SNE_Vx_Vy((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4);
			break;

		case 0xA000:
			opc_LD_I_addr(opcode & 0x0FFF);
			break;

		case 0xB000:
			opc_JP_V0_byte(opcode & 0x0FFF);
			break;

		case 0xC000:
			opc_RND((opcode & 0x0F00) >> 8, opcode & 0x00FF);
			break;

		case 0xD000:
			opc_DRW((opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4, opcode & 0x000F);
			break;

		case 0xE000:
			if((opcode & 0x00FF) == 0x9E)
				opc_SKP((opcode & 0x0F00) >> 8);
			else if((opcode & 0x00FF) == 0xA1)
				opc_SKNP((opcode & 0x0F00) >> 8);
			break;

		case 0xF000:
			switch(opcode & 0x00FF)
			{
				case 0x07:
					opc_LD_Vx_DT((opcode & 0x0F00) >> 8);
					break;

				case 0x0A:
					break;

				case 0x15:
					opc_LD_DT_Vx((opcode & 0x0F00) >> 8);
					break;

				case 0x18:
					opc_LD_ST_Vx((opcode & 0x0F00) >> 8);
					break;

				case 0x1E:
					opc_ADD_I_Vx((opcode & 0x0F00) >> 8);
					break;

				case 0x29:
					opc_LD_F_Vx((opcode & 0x0F00) >> 8);
					break;

				case 0x33:
					opc_LD_B_Vx((opcode & 0x0F00) >> 8);
					break;

				case 0x55:
					opc_LD_at_I_from_V0_to_Vx((opcode & 0x0F00) >> 8);
					break;

				case 0x65:
					opc_LD_at_V0_to_Vx_from_I((opcode & 0x0F00) >> 8);
					break;
			}
			break;
		default:
			printf("Unknown opcode 0x%.4X\n", opcode);
			break;
	}
}

int Chip8::get_multiple() const
{
	return multiple;
}

int Chip8::get_freq() const
{
	return freq;
}
