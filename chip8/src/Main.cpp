#include <iostream>

#include <SDL/SDL.h>

#include <Chip8.hpp>
#include <Timer.hpp>

using namespace std;

int main(int argc, char *argv[])
{
	Chip8 chip8;
	Timer timer_60Hz(60);
	Timer timer_exec(chip8.get_freq());
	SDL_Surface *screen = NULL;

	if(argc != 2)
	{
		cout << "Usage: " << argv[0] << " rom.ch8" << endl;
		return -1;
	}

	if(!chip8.load_rom(argv[1]))
	{
		cout << "Unable to load rom " << argv[0] << endl;
		return -1;
	}

	if(SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		fprintf(stderr, "Error initializing SDL: %s\n", SDL_GetError());
		return -1;
	}

	screen = SDL_SetVideoMode(64*chip8.get_multiple(), 32*chip8.get_multiple(), 32, SDL_SWSURFACE);
	if(!screen)
	{
		fprintf(stderr, "Error opening window: %s\n", SDL_GetError());
		SDL_Quit();
		return -1;
	}
	SDL_WM_SetCaption("Chip8 Emulator", NULL);

	//This infinite loop is temporary
	timer_60Hz.start();
	timer_exec.start();
	for(;;)
	{	
		if(timer_exec.next())
		{
			chip8.next_iter();
		}

		if(timer_60Hz.next())
		{
			chip8.refresh_screen(screen);
			SDL_Flip(screen);
		}

		if(chip8.is_quitev())
			break;
	}

	SDL_Quit();

	return 0;
}
