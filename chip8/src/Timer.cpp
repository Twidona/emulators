#include <SDL/SDL.h>

#include <Timer.hpp>

Timer::Timer(uint32_t freq)
{
	ms_p = (uint16_t) (((1./freq) * 1000.) + 0.5);
}

Timer::~Timer()
{

}

void Timer::start()
{
	current_time = last_time = SDL_GetTicks();
}


bool Timer::next()
{
	current_time = SDL_GetTicks();
	if(current_time - last_time >= ms_p)
	{
		#ifdef DEBUG
		printf("Time error: %dms\n", current_time - last_time - ms_p);
		#endif

		last_time = current_time;
		return true;
	}
	else
		return false;
}
