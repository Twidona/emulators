#include <cstring>

#include <SDL/SDL.h>

#include <Events.hpp>

Events::Events() : quitev(false)
{
	memset(keys, 0, 0x10);
}

Events::~Events()
{

}

void Events::poll()
{
	SDL_Event event;

	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_QUIT:
				quitev = true;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_KP0:
						keys[0x0] = 1;
						break;
					case SDLK_KP1:
						keys[0x7] = 1;
						break;
					case SDLK_KP2:
						keys[0x8] = 1;
						break;
					case SDLK_KP3:
						keys[0x9] = 1;
						break;
					case SDLK_KP4:
						keys[0x4] = 1;
						break;
					case SDLK_KP5:
						keys[0x5] = 1;
						break;
					case SDLK_KP6:
						keys[0x6] = 1;
						break;
					case SDLK_KP7:
						keys[0x1] = 1;
						break;
					case SDLK_KP8:
						keys[0x2] = 1;
						break;
					case SDLK_KP9:
						keys[0x3] = 1;
						break;
					case SDLK_RIGHT:
						keys[0xA] = 1;
						break;
					case SDLK_KP_PERIOD:
						keys[0xB] = 1;
						break;
					case SDLK_KP_MULTIPLY:
						keys[0xC] = 1;
						break;
					case SDLK_KP_MINUS:
						keys[0xD] = 1;
						break;
					case SDLK_KP_PLUS:
						keys[0xE] = 1;
						break;
					case SDLK_KP_ENTER:
						keys[0xF] = 1;
						break;
				}
				break;
			case SDL_KEYUP:
				switch(event.key.keysym.sym)
				{
					case SDLK_KP0:
						keys[0x0] = 0;
						break;
					case SDLK_KP1:
						keys[0x7] = 0;
						break;
					case SDLK_KP2:
						keys[0x8] = 0;
						break;
					case SDLK_KP3:
						keys[0x9] = 0;
						break;
					case SDLK_KP4:
						keys[0x4] = 0;
						break;
					case SDLK_KP5:
						keys[0x5] = 0;
						break;
					case SDLK_KP6:
						keys[0x6] = 0;
						break;
					case SDLK_KP7:
						keys[0x1] = 0;
						break;
					case SDLK_KP8:
						keys[0x2] = 0;
						break;
					case SDLK_KP9:
						keys[0x3] = 0;
						break;
					case SDLK_RIGHT:
						keys[0xA] = 0;
						break;
					case SDLK_KP_PERIOD:
						keys[0xB] = 0;
						break;
					case SDLK_KP_MULTIPLY:
						keys[0xC] = 0;
						break;
					case SDLK_KP_MINUS:
						keys[0xD] = 0;
						break;
					case SDLK_KP_PLUS:
						keys[0xE] = 0;
						break;
					case SDLK_KP_ENTER:
						keys[0xF] = 0;
						break;
				}
				break;
		}
	}
}

uint8_t Events::wait()
{
	SDL_Event event;

	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_QUIT:
				quitev = true;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym)
				{
					case SDLK_KP0:
						return 0x0;
					case SDLK_KP1:
						return 0x7;
					case SDLK_KP2:
						return 0x8;
					case SDLK_KP3:
						return 0x9;
					case SDLK_KP4:
						return 0x4;
					case SDLK_KP5:
						return 0x5;
					case SDLK_KP6:
						return 0x6;
					case SDLK_KP7:
						return 0x1;
					case SDLK_KP8:
						return 0x2;
					case SDLK_KP9:
						return 0x3;
					case SDLK_RIGHT:
						return 0xA;
					case SDLK_KP_PERIOD:
						return 0xB;
					case SDLK_KP_MULTIPLY:
						return 0xC;
					case SDLK_KP_MINUS:
						return 0xD;
					case SDLK_KP_PLUS:
						return 0xE;
					case SDLK_KP_ENTER:
						return 0xF;
				}
				break;
		}
	}

	return 0x10; //No key pressed
}

bool Events::is_pressed(uint8_t key)
{
	if(key > 0xF)
		return false;

	if(keys[key])
		return true;
	else
		return false;
}

bool Events::is_quitev()
{
	return quitev;
}
