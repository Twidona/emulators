#include <stdio.h>

#include <SDL/SDL.h>

bool ask_key(void);

int main()
{
	SDL_Surface *screen = NULL;
	SDL_Joystick **controller = NULL;
	bool ctrl_exists = false;

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) == -1)
	{
		fprintf(stderr, "Error initializing SDL: %s\n", SDL_GetError());
		return -1;
	}

	screen = SDL_SetVideoMode(1, 1, 32, SDL_SWSURFACE);
	if(!screen)
	{
		fprintf(stderr, "Error opening window: %s\n", SDL_GetError());
		SDL_Quit();
		return -1;
	}
	SDL_WM_SetCaption("keyconf", NULL);

	if(SDL_NumJoysticks() < 1)
	{
		printf("No controller\n");
	}
	else
	{
		printf("%u controller(s) detected\n", SDL_NumJoysticks());
		ctrl_exists = true;
	}

	if(ctrl_exists)
	{
		controller = new SDL_Joystick*[SDL_NumJoysticks()];
		printf("\n");
		for(int i = 0 ; i < SDL_NumJoysticks() ; i++)
		{
			printf("Joystick %u\n", i);
			printf("----------\n");
			printf("Name: %s\n", SDL_JoystickName(i));

			controller[i] = SDL_JoystickOpen(i);

			printf("Number of buttons: %u\n", SDL_JoystickNumButtons(controller[i]));
			printf("Number of hats: %u\n", SDL_JoystickNumHats(controller[i]));
			printf("Number of axes: %u\n", SDL_JoystickNumAxes(controller[i]));
			printf("Number of trackballs: %u\n", SDL_JoystickNumBalls(controller[i]));
			printf("\n");
		}

		SDL_JoystickEventState(SDL_ENABLE);
	}

	while(ask_key());

	for(int i = 0 ; i < SDL_NumJoysticks() ; i++)
	{
		SDL_JoystickClose(controller[i]);		
	}
	delete[] controller;

	SDL_Quit();
	return 0;
}

bool ask_key(void)
{
	SDL_Event event;

	int key = 0;

	while(key == 0)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case SDL_KEYDOWN:
				printf("Key %u pressed\n", event.key.keysym.sym);
				return true;
			case SDL_JOYBUTTONDOWN:
				printf("Key %u from controller %u pressed\n", event.jbutton.button, event.jbutton.which);
				return true;
			case SDL_JOYHATMOTION:
				printf("Hat %u moved ()\n", event.jhat.hat);
				return true;
			case SDL_JOYAXISMOTION:
				printf("Axis %u moved: %i\n", event.jaxis.axis, event.jaxis.value);
				return true;
			case SDL_QUIT:
				return false;

			//event.jaxis.value = the current position of the axis (range -32768 to 32767)
			//event.jaxis.which, event.jbutton.which, event.jhat.which
		}
	}
}
